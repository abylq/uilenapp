<?php

namespace App\Http\Resources;

use App\Models\Message;
use App\Models\Reaction;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UpdateMessageList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        $arr = [];
        $receiver = "";
        if ($this->sender_id) {
            $from = Message::where('sender_id', $this->sender_id)->orderBy('id','DESC')->first();
            $reaction = Reaction::where('id', $from->reaction_id);
            $reactionImage = null;
            if ($reaction->exists()) {
                $reactionImage = $reaction->first()->image;
            }
            $arr['sender'] = [
                'id'         => $from->id,
                //'receiver'   => User::where('id',$from->receiver_id)->select('id','name')->first(),
                'message'    => $from->message == "null" ? null : $from->message,
                'file'       => $from->file,
                'reaction'   => $reactionImage,
                'created_at' => $from->created_at->format('Y-m-d H:i:s'),

            ];
            $receiver = new UserReceiverSocketResource($from->receiverUser, $this->sender_id, $from->receiver_id);
        }
        if($this->receiver_id) {
            $to = Message::where('receiver_id', $this->receiver_id)->orderBy('id','DESC')->first();
            $reaction = Reaction::where('id', $to->reaction_id);
            $reactionImage = null;
            if ($reaction->exists()) {
                $reactionImage = $reaction->first()->image;
            }
            $arr['receiver'] = [
                'id' => $to->id,
                //'sender' => User::where('id',$to->sender_id)->select('id','name')->first(),
                'message'    => $to->message == "null" ? null : $to->message,
                'file'       => $to->file,
                'reaction'   => $reactionImage,
                'created_at' => $to->created_at->format('Y-m-d H:i:s'),

            ];
            $receiver = new UserReceiverSocketResource($to->senderUser, $to->sender_id, $this->receiver_id);
        }
        return [
            'last_message' => $arr,
            'receiver'     => $receiver,
        ];
    }
}
