@extends('admin.layouts.admin')

@section('title', $tariff->name)

@section('content')
    <div class="card">
        <div class="card-body">

            <form action="{{ route('admin.tariffs.update', $tariff->id) }}" enctype="multipart/form-data" method="post">
                @method('put')
                @csrf
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" required class="form-control" value="{{ $tariff->name }}">
                </div>
                <div class="form-group">
                    <label for="price">Цена</label>
                    <input type="number" min="1" name="price" id="price" required class="form-control" value="{{ $tariff->price }}">
                </div>

                <div class="form-group">
                    <label >Описание</label>
                    <input type="text" name="description"  required class="form-control" value="{{ $tariff->description }}">
                </div>



                <div class="form-group">
                    <button class="btn btn-success">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
@endsection
