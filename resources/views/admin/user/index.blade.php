@extends('admin.layouts.admin')
@section('title','Пользователи')
@section('topRightContent')
    <a href="#" class="btn btn-primary float-sm-right"> Отчеты</a>
@endsection
@section('content')


    <div class="card">
        <div class="card-body p-2">
            <table class="table table-bordered data-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ФИО</th>
                    <th>Номер телефона</th>

                    <th>Пол</th>
                    <th>Дата регистрации</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>



        </div>
    </div>
@endsection

@push('js')
    @include('admin.layouts.datatables')

    <script type="text/javascript">
        $(function () {

            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.users.list') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'phone', name: 'phone'},
                    {data: 'sex', name: 'sex',render: function (type) {
                            var typeUser = type == 'male' ? "Мужчина" : "Женщина";
                            return '<span class="badge badge-primary">'+ typeUser + '</span>';
                        }},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
@endpush
