<?php

namespace App\Http\Traits;

use App\Http\Resources\MessageResourceSocket;
use App\Http\Resources\UsersMatchedResource;
use App\Models\NotificationSetting;
use App\Models\User;
use denis660\Centrifugo\Centrifugo;
use OneSignal;
use Throwable;

trait FCMNotification
{

    public function sendFcmNotification($userId, $message, $body)
    {
        $firebaseToken = User::whereNotNull('app_client_id')->where('id', $userId)->pluck('app_client_id');
        if (count($firebaseToken) < 0) {
            return false;
        }

        $SERVER_API_KEY = "AAAAHLPwxWk:APA91bFwiXCTiSMZxZj4snO997UYb7ZtMfwXXOLz1zVVF0D9Mf529EOCy3UgMCi2DAJKvlTUz3k3K05AX7I7lAuFU4-ebRwIYRfPol2tWgXobuMGG0uofDb1AmJHBVoUBIXsEeuYtnOU";
        $data = [
            "registration_ids" => $firebaseToken,
            "notification" => [
                "title" => $message,
                "body" => $body,
            ]
        ];
        $dataString = json_encode($data);
        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        $response = curl_exec($ch);
        return $response;
    }

    public function sendNotificationHuawei($userId, $message, $body, $type)
    {
        return false;
    }

    private function pushKitToken()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://oauth-login.cloud.huawei.com/oauth2/v3/token');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "grant_type=client_credentials&client_id=105744451&client_secret=138135defd1c0d4eb1b81a0c06324d3efaba352bfb5897d6ad1d21277f547b0c",
        );
        $response = curl_exec($ch);
        $res = json_decode($response, true);
        return $res;
    }

    public function sendNotificationOneSignal($userId, $message, $body, $type)
    {
        $firebaseToken = User::whereNotNull('app_client_id')->where('id', $userId);
        if (!$firebaseToken->exists()) {
            return false;
        }
        if(!NotificationSetting::getStatus($userId)) {
            return false;
        }
        
        try {
            $datA = null ;
            if($type == 'message') {
                $datA = response()->json([
                    'success' => true,
                    'data' => new MessageResourceSocket($body),
                    'type' => $type,
                    'message' => $body->message
                ])->getData();
            }
            if($type == 'reaction') {
                $datA = response()->json([
                    'success' => true,
                    'data' => new MessageResourceSocket($body),
                    'type' => $type,
                ])->getData();
            }
            if($type == 'like' || $type == 'matched') {
                $datA = response()->json([
                    'success' => true,
                    'data' => new UsersMatchedResource($body),
                    'type' => $type,
                ])->getData();
            }  

            $signal = OneSignal::sendNotificationToUser(
                $message,
                $firebaseToken->first()['app_client_id'],
                $url = null,
                $data = $datA,
                $buttons = null,
                $schedule = null,
                $headings = $message
            );
        }catch(Exception $th) {
            return $th;
        }
        return true;
        
    }
}


