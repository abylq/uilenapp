<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(LoginRequest $request): JsonResponse
    {
        if (!Auth::attempt([
            'phone' => $request->phone,
            'password' => $request->password]))
        {
            return response()->json([
                'message' => __('auth.failed')
            ], Response::HTTP_BAD_REQUEST);
        }
        if($request->get('app_client_id')) {
            auth()->user()->update([
                'app_client_id' => $request->get('app_client_id')
            ]);
        }
        $addSettings = (new User())->setNotificationSettings($request, auth()->id(), 'auth');
        return response()->json([
            'message' => 'Вы успешно авторизовались',
            'user' => new UserResource(auth()->user()),
            'token' => auth()->user()->createToken('user-token')->plainTextToken,
            'lang'  => app()->getLocale(),
        ]);
    }
}
