@extends('admin.layouts.admin')

@section('title','Главная страница')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">


                            <h3 class="profile-username text-center">Новые пользователи </h3>

                            <p class="text-muted text-center"></p>

                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>За сутки</b> <a class="float-right">{{ \App\Models\User::report('day')->count() }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>За неделю</b> <a class="float-right">{{ \App\Models\User::report('week')->count() }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>За месяц</b> <a class="float-right">{{ \App\Models\User::report('month')->count() }}</a>
                                </li>

                            </ul>

                        </div>
                        <!-- /.card-body -->
                    </div>


                </div>

                <div class="col-md-3">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">


                            <h3 class="profile-username text-center">Сообщенио</h3>

                            <p class="text-muted text-center"></p>

                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>За сутки</b> <a class="float-right"> {{ \App\Models\Message::report('day')->count() }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>За неделю</b> <a class="float-right">{{ \App\Models\Message::report('week')->count() }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>За месяц</b> <a class="float-right">{{ \App\Models\Message::report('month')->count() }}</a>
                                </li>
                            </ul>

                        </div>
                        <!-- /.card-body -->
                    </div>


                </div>
                <div class="col-md-3">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">


                            <h3 class="profile-username text-center">Популярные реакции</h3>

                            <p class="text-muted text-center"></p>

                            <ul class="list-group list-group-unbordered mb-3">
                                @if($reactions->count() > 0)
                                    @foreach($reactions as $reaction)
                                        <li class="list-group-item">
                                            <b><img src="{{ asset($reaction->image) }}" width="30"></b> <a class="float-right"> {{ $reaction->aggregate }}</a>
                                        </li>
                                    @endforeach
                               @endif
                            </ul>

                        </div>
                        <!-- /.card-body -->
                    </div>


                </div>
                <!-- /.col -->

                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@endsection
