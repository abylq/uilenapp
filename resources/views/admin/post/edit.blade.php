@extends('admin.layouts.admin')


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{route('admin.post.update')}}">
                                <input type="hidden" name="post_id" value="{{$post->id}}">
                                @csrf
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 col-form-label">названия</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="title" required  class="form-control" id="inputName" value="{{$post->title}}" placeholder="названия">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="image" class="col-sm-2 col-form-label">Фото</label>

                                    <div class="col-sm-10">
                                        <img  src="{{asset($post->image)}}" width="100" alt="">
                                        <br>
                                        <br>
                                        <input type="file" name="image"  class="form-control" id="image" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="description" class="col-sm-2 col-form-label">текст</label>
                                    <div class="col-sm-10">
                                        <textarea name="text" id="description"  class="form-control">{{$post->text}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger">Сохранить</button>
                                    </div>
                                </div>
                            </form>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@endsection
