<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tariff;
use App\Models\TariffDetail;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TariffDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tariff.detail-create',[
            'tariff' => Tariff::find(request()->id),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id' => 'required|exists:tariffs,id',
            'detail' => 'required|array',
        ]);

        foreach ($request->detail as $key => $value) {

            if ($value) {
                TariffDetail::create([
                    'tariff_id' => $request->id,
                    'name' => $value
                ]);
            }
        }
        return redirect()->back()->with('success','Успешно добавлено');
    }

    /**
     * @param int $id
     * @return View
     */
    public function show($id): View
    {

        return \view('admin.tariff.detail-show', [
            'tariff' => Tariff::find($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return \view('admin.tariff.tariff-detail-edit', [
            'tariff_detail' => TariffDetail::find($id)
        ]);
    }

    /**
     * @param Request $request
     * @param TariffDetail $tariffDetail
     */
    public function update(Request $request, TariffDetail $tariffDetail)
    {
        $request->validate([
            'title' => 'required'
        ]);

        if ($tariffDetail) {
            $tariffDetail->update(['name' => $request->title]);
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
