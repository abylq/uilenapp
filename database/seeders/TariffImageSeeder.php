<?php

namespace Database\Seeders;

use App\Models\ImageTariff;
use Illuminate\Database\Seeder;

class TariffImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ImageTariff::create([
            'tariff_id' => 1,
            'title' => 'Узнай, кому ты нравишься',
            'description' => 'Получи доступ к списку людей, которые уже поставили тебе лайк',
            'image' => 'images/tariff/SoftLight.png',
        ]);

        ImageTariff::create([
            'tariff_id' => 1,
            'title' => 'Узнай, кому ты понравилось',
            'description' => 'Получи доступ к списку людей, которые уже поставили тебе лайк',
            'image' => 'images/tariff/SoftLight.png',
        ]);
    }
}
