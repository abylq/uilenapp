@extends('admin.layouts.admin')

@section('title', $city->name)

@section('content')
    <div class="card">
        <div class="card-body">

            <form action="{{ route('admin.countries.update-city', $city->id) }}" enctype="multipart/form-data" method="post">
                @csrf

                <div class="form-group">
                    <label for="name">Область</label>
                    <select name="region_id" class="form-control">
                        @foreach($regions as $item)
                            <option value="{{ $item->id }}" {{ $city->region_id == $item->id ? 'selected': '' }}>{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" required class="form-control" value="{{ $city->name }}">
                </div>

                <div class="form-group">
                    <button class="btn btn-success">Создать</button>
                </div>
            </form>
        </div>
    </div>
@endsection
