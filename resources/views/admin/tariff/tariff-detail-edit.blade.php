@extends('admin.layouts.admin')

@section('title', $tariff_detail->name)

@section('content')
    <div class="card">
        <div class="card-body">

            <form action="{{ route('admin.tariff_details.update', $tariff_detail->id) }}" enctype="multipart/form-data" method="post">
                @method('put')
                @csrf
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="title" id="name" required class="form-control" value="{{ $tariff_detail->name }}">
                </div>

                <div class="form-group">
                    <button class="btn btn-success">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
@endsection
