@extends('admin.layouts.admin')

@section('title','Профиль')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            @if($user->avatar)
                                <div class="text-center">
                                    <img class="profile-user-img img-fluid img-circle" src="" alt="User profile picture">
                                </div>
                            @endif

                            <h3 class="profile-username text-center">{{$user->name}}</h3>

                            <p class="text-muted text-center"></p>

                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>ID</b> <a class="float-right">{{$user->id}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Телефон номер</b> <a class="float-right">{{$user->phone}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>отзывы</b> <a class="float-right"></a>
                                </li>
                            </ul>

                        </div>
                        <!-- /.card-body -->
                    </div>


                </div>

                <div class="col-md-6">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            @if($user->avatar)
                                <div class="text-center">
                                    <img class="profile-user-img img-fluid img-circle" src="" alt="User profile picture">
                                </div>
                            @endif

                            <h3 class="profile-username text-center">Детали</h3>

                            <p class="text-muted text-center"></p>

                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>ID</b> <a class="float-right">{{$user->id}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Телефон номер</b> <a class="float-right">{{$user->phone}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>отзывы</b> <a class="float-right"></a>
                                </li>
                            </ul>

                        </div>
                        <!-- /.card-body -->
                    </div>


                </div>
                <!-- /.col -->

                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@endsection
