<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Interest;
use App\Models\User;
use App\Models\UserAbout;
use App\Models\UserDetail;
use App\Models\UserImage;
use App\Models\UserInterest;
use App\Models\UserLocation;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $sex = ['male','female'];
        for($i = 0; $i< 1000; $i++) {
            $user = User::create([
                'name' => $this->makeRandomString(5),
                'email' => $this->makeRandomString(5).'@gmail.com',
                'password' => bcrypt(1234),
                'role' => 'user',
                'phone' => '702'.rand(1111111,9999999),
            ]);

            UserDetail::create([
                'user_id'       => $user->id,
                'city_id'        => City::inRandomOrder()->first()->id,
                'date_of_birth'  => date('Y-m-d',strtotime('2000-03-11')),
                'gener_id'       => null,
                'sex'            => $sex[array_rand($sex, 2)[0]],
                'me_status'      => $sex[array_rand($sex, 2)[0]] == 'male' ? 'Не женат' : 'Не замужем',
                'marital_status' => 'В активном поиске ',
                'about'          => null,
                'k_mal'          => rand(111111,666666),
                'bad_habits'     => 'Курение',
                'zodiac'         => 'Овен',
                'height'         => rand(111,999),
                'weight'         => rand(11,99),
            ]);

            UserAbout::create([
                'user_id' => $user->id,
                'salary'  => 1000000,
                'about'   => 'Красавица'
            ]);
            $images = ['images/users/9kowWYJ2jGtMBpUxAFmG.jpeg','images/users/e3arCKm5feP2VzjpaCwW.jpeg ','images/users/d8rPhWShRkEGVomUWWvQ.jpg'];
            foreach ($images as $image) {
                UserImage::create([
                    'user_id' => $user->id,
                    'images'  => $image
                ]);
            }

            $interest = Interest::all();

            foreach ($interest as $inter) {
                UserInterest::create([
                    'interest_id' => $inter->id,
                    'user_id'     => $user->id,
                ]);
            }
            if ($i == 0) {
                UserLocation::create([
                    'user_id'   => $user->id,
                    'latitude' => 43.21976679157426,
                    'longitude' => 76.91926896572113,
                ]);
            }
            if ($i == 1) {
                UserLocation::create([
                    'user_id'   => $user->id,
                    'latitude' => 43.21969642547784,
                    'longitude' => 76.92142546176912,
                ]);
            }
            if ($i == 2) {
                UserLocation::create([
                    'user_id'   => $user->id,
                    'latitude' => 43.2218933730633,
                    'longitude' => 76.92352294921876,
                ]);
            }
            if ($i != 0 && $i != 1 && $i != 2) {
                UserLocation::create([
                    'user_id'   => $user->id,
                    'latitude' => 43.2218933730633,
                    'longitude' => 76.92352294921876,
                ]);
            }

        }


        
 


    }

    private function makeRandomString($max=6) {
        $i = 0; //Reset the counter.
        $possible_keys = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $keys_length = strlen($possible_keys);
        $str = ""; //Let's declare the string, to add later.
        while($i<$max) {
            $rand = mt_rand(1,$keys_length-1);
            $str.= $possible_keys[$rand];
            $i++;
        }
        return $str;
    }
}
