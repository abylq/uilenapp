<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\Matching;
use App\Models\User;
use Illuminate\Http\Request;

class MatchController extends Controller
{


   public function match()
   {
        $matching = Matching::where(fn($q) => $q
            ->where('to_user_id', auth()->id())
            ->Orwhere('from_user_id', auth()->id())
            
        )->where('matched', 1);
        $userIds = [];
        foreach($matching->get() as $match) {
             $userIds[] = $match->to_user_id;
             $userIds[] = $match->from_user_id;
        }
        foreach (array_keys($userIds, auth()->id(), true) as $key) {
            unset($userIds[$key]);
        }
      
       $matchingUsers = User::whereIn('id', $userIds)->get();
       $matchingUsersCount = count($matchingUsers);

       return response()->json([
           'data' => UserResource::collection($matchingUsers)
       ]);
   }
}
