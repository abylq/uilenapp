<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Conversation extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function createConversation(int $receiverID, string $date)
    {
        $checkConversation1 = self::where([
            'sender_id'     => auth()->id(),
            'receiver_id'   => $receiverID
        ])->whereDate('created_at', $date);

        if ($checkConversation1->exists()) {
            return $checkConversation1->first();
        }
        $checkConversation2 = self::where([
            'sender_id'     => $receiverID,
            'receiver_id'   => auth()->id(),
        ])->whereDate('created_at', $date);

        if ($checkConversation2->exists()) {
            $checkConversation2->update([
                'updated_at' => Carbon::now()
            ]);
            return $checkConversation2->first();
        }

        $conversation = self::create([
            'sender_id'     => auth()->id(),
            'receiver_id'   => $receiverID,
            'date_message'    => $date,
        ]);

        return $conversation;
    }

    /**
     * @return HasMany
     */
    public function messages(): HasMany
    {
        return $this->hasMany(Message::class);
    }

    /**
     * @return BelongsTo
     */
    public function receiverUser(): BelongsTo
    {
        return $this->belongsTo(User::class,'receiver_id');
    }
}
