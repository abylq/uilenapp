<?php

namespace App\Models;

use App\Http\Resources\UserDetailResource;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Http\Response;

class UserDetail extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }


    public function setEachDetails(object $request)
    {
        $userDetail = UserAbout::where('user_id', auth()->id());

        if ($userDetail->exists()) {
            if ($request->about) {
                $userDetail->update(['about' => $request->get('about')]);
            }

            if ($request->salary) {
                $userDetail->update(['salary' => $request->get('salary')]);
            }

            return response()->json([
                'data' => new UserDetailResource($userDetail->first())
            ]);
        }else {
            UserAbout::create([
                'user_id' => auth()->id(),
                'about'   => $request->about
            ]);

            return response()->json([
                'message' => 'Successfully created'
            ], Response::HTTP_OK);
        }

    }




}
