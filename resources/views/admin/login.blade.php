@extends('admin.layouts.base')
@section('theme','login-page')
@section('wrapper')
    <div class="login-box">
        <div class="login-logo">
            <b>UILEN</b>
        </div>
        <div class="card">
            <div class="card-body login-card-body">

                @if($errors->any())
                    @foreach ($errors->all() as $error)
                        <p class="login-box-msg">{{$error}}</p>
                    @endforeach

                @else
                    <p class="login-box-msg">Введите данные</p>
                @endif
                <form action="{{route('auth')}}" method="POST">
                    @csrf
                    <div class="input-group mb-3">
                        <input type="text" name="email" required class="form-control" placeholder="логин">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="password" required class="form-control" placeholder="пароль">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary btn-block">Войти</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->
@endsection
