<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{

    public function __invoke(Request $request)
    {
        $request->validate([
            'password' => 'required'
        ]);
        try {
            $user = User::find(auth()->user()->id);
            $user->password = bcrypt($request->password);
            $user->save();
        }catch (Exception $exception) {
            return response()->json([
                'error' => true,
                'message' => 'Пожалуйста повторите еще раз',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error' => false,
            'message' => 'Ваш пароль успешно изменен',
        ]);
    }
}
