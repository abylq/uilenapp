<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LogoutController extends Controller
{

    /**
     * @return JsonResponse
     */
    public function __invoke(): JsonResponse
    {
        try {
            auth()->user()->currentAccessToken()->delete();
        } catch (Throwable $th) {
            return response()->json([
                'message' => 'Unable to delete user token!'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'message' => 'Вы вышли из системы'
        ], Response::HTTP_OK);
    }
}
