@extends('admin.layouts.admin')

@section('title','Добавить ')

@section('content')
    <div class="card">
        <div class="card-body">

            <form action="{{ route('admin.countries.store-countries') }}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" required class="form-control">
                </div>

                <div class="form-group">
                    <button class="btn btn-success">Создать</button>
                </div>
            </form>
        </div>
    </div>
@endsection
