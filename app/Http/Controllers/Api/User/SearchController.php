<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\PaginationResource;
use App\Http\Resources\SearchCollection;
use App\Http\Resources\SearchResource;
use App\Http\Resources\UserResource;
use App\Http\Services\Search\Search;
use App\Http\Services\Search\SearchUsers;
use App\Models\RecentlySearch;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SearchController extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function search(Request $request)
    {
        $request->validate([
            'name' => 'string',
            'city_id' => 'exists:cities,id',
        ]);
        try {
            $search = new Search($request);
            $search = new SearchUsers($search);
            $search = $search->getQuery();
        }catch (\Throwable $th) {
            return response()->json([
                'message' => 'Ошибка '.$th
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

       return new PaginationResource($search->paginate(10), SearchResource::class);


    }

    /**
     * @return JsonResponse
     */
    public function getRecently(): JsonResponse
    {
        $recentLySaerch = RecentlySearch::where('user_id', auth()->id())->get();
        $userIds = [];
        foreach($recentLySaerch as $search) {
             $userIds[] = $search->search_user_id;
        }
        if ($recentLySaerch) {
            return response()->json([ 
                'data' => UserResource::collection(User::whereIn('id', $userIds)->get())
            ]);
        }
        return response()->json([
            'data' => []
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function postRecently(Request $request): JsonResponse
    {
        // Request user_id we use for search_user_id column
        $request->validate([
            'user_id' => 'required|exists:users,id'
        ]);

        try {
            RecentlySearch::create([
                'user_id' => auth()->id(),
                'search_user_id' => $request->get('user_id')
            ]);
        }catch (\Throwable $th) {
            return response()->json([
                'message' => 'Не удалось добавить в базу данных'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'message' => 'Successfully'
        ], Response::HTTP_OK);
    }

    /**
     * @param User $user
     * @return JsonResponse
     */
    public function deleteRecently(User $user): JsonResponse
    {
        $delete = RecentlySearch::where('user_id', auth()->id())->where('search_user_id', $user->id);
        if ($delete->exists()) {
            $delete->delete();

            return response()->json([
                'message' => 'Успешно удалено'
            ]);
        }

        return response()->json([
            'message' => 'Пожалуйста повторите еще раз'
        ], Response::HTTP_BAD_REQUEST);
    }
}
