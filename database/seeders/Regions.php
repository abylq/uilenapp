<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Region;
use Illuminate\Database\Seeder;

class Regions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = Country::create([
            'name' => 'Kazakstan'
        ]);
        $region = Region::create([
            'name' => 'Атырауская область',
            'country_id' => 1
        ]);
        $region = Region::create([
            'name' => 'Северная Казахстанская область',
            'country_id' => 1
        ]);
        $region = Region::create([
            'name' => 'Алматинский область',
            'country_id' => 1
        ]);
    }
}
