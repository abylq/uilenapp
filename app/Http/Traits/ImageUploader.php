<?php

namespace App\Http\Traits;

use Illuminate\Support\Str;

trait ImageUploader
{

    /**
     * @param $query
     * @param string $path
     * @return string
     */
    public function upload($query, $path = '')
    {
        $imageFulName = Str::random(20).'.'.strtolower($query->getClientOriginalExtension());
        $upload_path = 'images/'.$path;
        $imageUrl = $upload_path.$imageFulName;
        $query->move($upload_path,$imageFulName);
        return $imageUrl;
    }
}
