@extends('admin.layouts.admin')

@section('title','Добавить новый тариф')

@section('content')
    <div class="card">
        <div class="card-body">

            <form action="{{ route('admin.tariffs.store') }}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" required class="form-control">
                </div>
                <div class="form-group">
                    <label for="price">Цена</label>
                    <input type="number" min="1" name="price" id="price" required class="form-control">
                </div>

                <div class="form-group">
                    <label for="description">Описание</label>
                    <input type="text" name="description" id="description" required class="form-control">
                </div>

                <div class="form-group">
                    <button class="btn btn-success">Создать</button>
                </div>
            </form>
        </div>
    </div>
@endsection
