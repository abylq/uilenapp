<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Testing\Fluent\Concerns\Has;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable,SoftDeletes;

    public const BANNED = 1;
    public const UNBANNED = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return HasOne
     */
    public function detail(): HasOne
    {
        return $this->hasOne(UserDetail::class);
    }

    /**
     * @return HasMany
     */
    public function interests(): HasMany
    {
        return $this->hasMany(UserInterest::class);
    }

    /**
     * @return HasMany
     */
    public function images(): HasMany
    {
        return $this->hasMany(UserImage::class)->orderBy('avatar','DESC');
    }

    /**
     * @return HasOne
     */
    public function location(): HasOne
    {
        return $this->hasOne(UserLocation::class)->latest();
    }

    /**
     * @return HasMany
     */
    public function messages(): HasMany
    {
        return $this->hasMany(Message::class, 'sender_id', 'id');
    }

    /**
     * @param int $userId
     * @param int $limit
     * @param string $orderBy
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getMessagesWith(int $userId, int $limit = 0, string $orderBy = 'ASC')
    {
        $messages = $this->messages()
            ->where(fn($q) => $q
                ->where('receiver_id', $userId)
                ->where('sender_id', $this->id))
            ->orWhere(fn($q) => $q
                ->where('receiver_id', $this->id)
                ->where('sender_id', $userId))
            ->orderBy('created_at', $orderBy);

        if ($limit) {
            $messages->limit($limit);
        }

        return $messages->paginate(10);
    }

    public static function report(string $date)
    {
        $users = User::where('role', 'user');

        switch ($date) {
            case 'month':
                $users->whereMonth('created_at', Carbon::now()->month);
                break;
            case 'week':
                $startOfWeek = Carbon::now()->startOfWeek();
                $endOfWeek = Carbon::now()->endOfWeek();
                $users->where('created_at', '>=', $startOfWeek)
                    ->where('created_at', '<=', $endOfWeek);
                break;
            case 'day':
                $users->whereDate('created_at', Carbon::today());
                break;
        }

        return $users;
    }


    public function checkTimeUserTariff()
    {
        $earlier = date('d-m-Y');
        $later = new DateTime("2022-07-09");

        $abs_diff = $later->diff($earlier)->format("%a"); //3

    }

    /**
     * @return string
     */
    public static function sexStatus(): string
    {
        $user = User::where('id', auth()->id())->with('detail')
            ->first();
            
        return $user->detail['sex'] == 'male' ? 'female' : 'male';
    }

    /**
     * @return HasOne
     */
    public function about(): HasOne
    {
        return $this->hasOne(UserAbout::class);
    }

    /**
     * @return HasMany
     */
    public function reactions(): HasMany
    {
        return $this->hasMany(UserReaction::class);
    }

    /**
     * @param object $request
     */
    public function editProfile(object $request)
    {
        $user = self::where('id', auth()->id())->first();

        $user->update([
            'name' => $request->get('name') ?? $user->name,
        ]);
        $userDetail = UserDetail::where('user_id', $user->id);
        if ($userDetail->exists()) {
            $user->detail->update([
                'city_id'        => $request->get('city_id') ?? $user->detail['city_id'],
                'date_of_birth'  => $request->get('date_of_birth') ?? $user->detail['date_of_birth'],
                'gener_id'       => $request->get('gener_id') ?? $user->detail['gener_id'],
                'sex'            => $request->get('sex') ?? $user->detail['sex'],
                'me_status'      => $request->get('me_status') ?? $user->detail['me_status'],
                'marital_status' => $request->get('marital_status') ?? $user->detail['marital_status'],
                'k_mal'          => $request->get('k_mal', $user->detail['k_mal']),
                'bad_habits'     => $request->get('bad_habits', $user->detail['bad_habits']),
                'zodiac'         => $request->get('zodiac', $user->detail['zodiac']),
                'height'         => $request->get('height', $user->detail['height']),
                'weight'         => $request->get('weight', $user->detail['weight']),

            ]);

        }else {
            UserDetail::create([
                'user_id'           => $user->id,
                'city_id'           => $request->get('city_id'),
                'date_of_birth'     => $request->get('date_of_birth'),
                'gener_id'          => $request->get('gener_id'),
                'sex'               => $request->get('sex'),
                'me_status'         => $request->get('me_status'),
                'marital_status'    => $request->get('marital_status'),
                'k_mal'             => $request->get('k_mal'),
                'bad_habits'        => $request->get('bad_habits'),
                'zodiac'            => $request->get('zodiac'),
                'height'            => $request->get('height'),
                'weight'            => $request->get('weight'),
            ]);


        }
        $aboutUser = UserAbout::where('user_id', $user->id);
        if ($aboutUser->exists()) {
            $user->about->update([
                'about'          => $request->get('about') ?? $user->about['about'],
                'salary'         => $request->get('salary') ?? $user->about['salary'],
            ]);
        }else {
            UserAbout::create([
                'user_id'        => $user->id,
                'about'          => $request->get('about'),
                'salary'         => $request->get('salary'),
            ]);
        }
        if ($request->interests) {
            $check = UserInterest::where('user_id', auth()->id());
            if ($check->exists()) {
                $check->delete();
                foreach ($request->interests as $key => $value) {
                    UserInterest::create([
                        'user_id'       => auth()->id(),
                        'interest_id'   => $value
                    ]);
                }
            }else {
                foreach ($request->interests as $key => $value) {
                    UserInterest::create([
                        'user_id'       => auth()->id(),
                        'interest_id'   => $value
                    ]);
                }
            }

        }


        return $user;
    }

    /**
     * @return BelongsTo
     */
    public function toUserId(): BelongsTo
    {
        return $this->belongsTo(Matching::class, 'to_user_id');
    }

    /**
     * @return BelongsTo
     */
    public function fromUserId() : BelongsTo
    {
        return $this->belongsTo(Matching::class, 'from_user_id');
    }

    public function notificationSettings(): HasMany 
    {
        return $this->hasMany(NotificationSetting::class);
    }

    public static function getUsersWithoutLiked(): array
    {
        $matchingCheck = Matching::where(fn($q) => $q
                ->where('to_user_id', auth()->id())
                ->Orwhere('from_user_id', auth()->id())
        );

        $userIds = [];
        foreach($matchingCheck->get() as $match) {
             $userIds[] = $match->to_user_id;
             $userIds[] = $match->from_user_id;
        }
        foreach (array_keys($userIds, auth()->id(), true) as $key) {
            unset($userIds[$key]);
        }

        return $userIds;
    }

    public function setNotificationSettings(object $request, $userId, $login = null)
    {
        $notification = NotificationSetting::where('user_id', $userId);
        if($notification->exists()) {
            if($login == null) {
                $notification->update([
                    'status' => $request->status
                ]);
            }
            return $notification->first();
        }

        $add = NotificationSetting::create([
            'user_id' => $userId,
            'status'  => $request->get('status',1),
        ]);

        return $add;
    }

    public function scopeDistance($query)
    {
        $lat = 43.238949;
        $long = 76.889709;
        $distance=0;
        $user = UserLocation::where('user_id', auth()->id());
            if($user->exists()) {
                $distance = $user->first()->distance ?? 200;
                $raw = DB::raw('ROUND ( ( 6371 * acos( cos( radians('.$user->first()->latitude.') ) * cos( radians( user_locations.latitude ) ) * cos( radians( user_locations.longitude ) - radians('.$user->first()->longitude.') ) + sin( radians('.$user->first()->latitude.') ) * sin( radians( user_locations.latitude ) ) ) ) ) AS rDistance');
            }else {
                $raw = DB::raw('ROUND ( ( 6371 * acos( cos( radians('.$lat.') ) * cos( radians( user_locations.latitude ) ) * cos( radians( user_locations.longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( user_locations.latitude ) ) ) ) ) AS rDistance');
            }
        return $query->addSelect($raw)->orderBy( 'rDistance', 'DESC' )->having('rDistance', '<=', $distance);

    }



}
