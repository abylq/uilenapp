<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\EmptyResourceActive;
use App\Http\Resources\LikeActiveResource;
use App\Models\Matching;
use App\Models\Tariff;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LikeActivesController extends Controller
{


    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $user = User::find(auth()->id());
        return response()->json([
            'data' => new LikeActiveResource($user)
        ]);
    }

    private function checkTariff()
    {
        $tariff = Tariff::where('type', 'wholike')->with(['userTariff' => function($query) {
            $query->where('user_id', auth()->id());
        }]);
    }
}
