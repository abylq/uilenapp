@extends('admin.layouts.admin')
@section('title', $tariff->name)
@section('topRightContent')
    <a href="{{ route('admin.tariff_details.create','id='.$tariff->id) }}" class="btn btn-primary float-sm-right">Добавить детали</a>
@endsection
@section('content')
    <div class="card">
        <div class="card-body p-0">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>название</th>

                    <th >действие</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($tariff->details as $tariff)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $tariff->name }}</td>
                            <td >
                                <a href="{{ route('admin.tariff_details.edit', $tariff->id) }}" class="btn btn-warning">
                                    <ion-icon name="pencil-outline"></ion-icon>
                                </a>

                                <a href="#" onclick="return confirm('Вы уверены ?')" class="btn btn-danger">
                                    <ion-icon name="trash-outline"></ion-icon>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>


        </div>
    </div>
@endsection
