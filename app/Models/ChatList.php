<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatList extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function checkChatList($receiverID)
    {
        $checkConversation1 = self::where([
            'sender_id'     => auth()->id(),
            'receiver_id'   => $receiverID
        ]);

        if ($checkConversation1->exists()) {
            $checkConversation1->update([
                'sender_id' => auth()->id(),
                'sended' => auth()->id(),
                'receiver_id' => $receiverID
            ]);
            return $checkConversation1->first();
        }
        $checkConversation2 = self::where([
            'sender_id'     => $receiverID,
            'receiver_id'   => auth()->id(),
        ]);

        if ($checkConversation2->exists()) {
            $checkConversation2->update([
                'sender_id' => auth()->id(),
                'sended' => auth()->id(),
                'receiver_id' => $receiverID
            ]);
            return $checkConversation2->first();
        }

        $conversation = self::create([
            'sender_id'     => auth()->id(),
            'receiver_id'   => $receiverID,
            'sended'        => auth()->id(),
        ]);

        return $conversation;
    }
}
