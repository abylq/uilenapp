<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Yajra\DataTables\DataTables;

class UsersController extends Controller
{


    public function index(Request $request)
    {
        if ($request->ajax()) {
            $users = User::where('role','user')->get();
            return Datatables::of($users)
                ->addIndexColumn()
                ->addColumn('created_at', function ($cred) {
                    return $cred->created_at->format('d-m-Y');
                })
                ->addColumn('action', function ($row) {
                    $btn = '<a href="' . route('admin.users.show', $row->id) . '" class="edit btn btn-warning btn-sm">
                            <ion-icon name="pencil-outline"></ion-icon>
                    </a>';
                    $message = "Вы уверены?";
                    $btn = $btn.' <a href="'.route('admin.users.destroy', $row->id).'" onclick="return confirm('.$message.')" class="btn btn-danger">
                                    <ion-icon name="trash-outline"></ion-icon>
                                </a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }


        return \view('admin.user.index');
    }

    /**
     * @param User $user
     * @return View
     */
    public function show(User $user): View
    {
        return \view('admin.user.show',[
            'user' => $user
        ]);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return RedirectResponse
     */
    public function banManage(Request $request, User $user): RedirectResponse
    {

        if ($user) {

            $user->update(['ban_status' => $request->ban_status == 1
                                        ? User::BANNED : User::UNBANNED]);
            return redirect();
        }
        return redirect();
    }

    /**
     * @param User $user
     * @return RedirectResponse
     */
    public function destroy(User $user): RedirectResponse
    {

        if ($user) {
            $user->delete();
            return redirect()->back()->with('success','Успешно удалено');
        }
        return redirect();
    }


}
