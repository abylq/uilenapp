@extends('admin.layouts.admin')

@section('title', $gener->name)

@section('content')
    <div class="card">
        <div class="card-body">

            <form action="{{ route('admin.geners.update', $gener->id) }}" enctype="multipart/form-data" method="post">
                @method('put')
                @csrf
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" required class="form-control" value="{{ $gener->name }}">
                </div>
                <div class="form-group">
                    @if($gener->parent_id != 0)
                        <input type="hidden" name="under_geners" value="1">
                        <label for="name">Выберите ру</label>
                        <select name="parent_id" class="form-control">
                            @foreach($geners as $item)
                                <option value="{{ $item->id }}" {{ $item->id == $gener->parent_id ? 'selected' : '' }}> {{ $item->name }}</option>
                            @endforeach
                        </select>
                    @else
                        <label for="name">Выберите жузы</label>
                        <select name="zhuz" class="form-control">
                            <option value="1" @if($gener->zhuz_id == 1) selected @endif>Улы жуз</option>
                            <option value="2" @if($gener->zhuz_id == 2) selected @endif>Орта жуз</option>
                            <option value="3" @if($gener->zhuz_id == 3) selected @endif>Кышы жуз</option>
                        </select>
                    @endif
                </div>
                <div class="form-group">
                    <button class="btn btn-success">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
@endsection
