<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gener;
use App\Models\Zhuz;
use Illuminate\Http\Request;
use Illuminate\View\View;

class GenersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ulu = Zhuz::withCount('geners')->get();

        return \view('admin.geners.zhuz',[
          'zhuz' => $ulu
        ]);
    }
    public function listByZhuz(Zhuz $zhuz)
    {
       return \view('admin.geners.index', [
            'geners' => Gener::where('parent_id', 0)->where('zhuz_id', $zhuz->id)->withCount('geners')->get(),
            'zhuz' => $zhuz
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (\request()->get('create') == 'under-geners') {
            return \view('admin.geners.create-under', [
                'geners' => Gener::where('parent_id', 0)->get()
            ]);
        }
        return view('admin.geners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
             'name' => 'required',
         ]);
         if ($request->under_geners == 1) {
             Gener::create([
                 'name' => $request->name,
                 'zhuz' => null,
                 'parent_id' => $request->geners,
             ]);

         }else {
             Gener::create([
                 'name' => $request->name,
                 'zhuz_id' => $request->zhuz,
                 'parent_id' => 0,
             ]);
         }
        return redirect()->back()->with('success','Успешно добавлено');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return \view('admin.geners.under-geners',[
            'geners' => Gener::where('id', $id)->with('geners')->first()
        ]);
    }

    /**
     * @param Gener $gener
     * @return View
     */
    public function edit(Gener $gener): View
    {
        if ($gener->parent_id != 0) {
            return \view('admin.geners.edit', [
                'geners' => Gener::where('parent_id', 0)->get(),
                'gener' => $gener
            ]);
        }
        return view('admin.geners.edit',[
            'gener' => $gener
        ]);
    }

    /**
     * @param Request $request
     * @param Gener $gener
     */
    public function update(Request $request, Gener $gener)
    {
        if ($gener) {
            $gener->update([
                'name' => $request->name,
                'parent_id' => $request->parent_id ?? 0,
                'zhuz_id'   => $request->zhuz_id
            ]);
            return redirect()->back()->with('success','Успешно обновлено');
        }
        return redirect()->back()->with('error','Повторите еще раз');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $gener = Gener::find($id);

        if ($gener) {
            $gener->delete();

            return redirect()->back()->with('success','Успешно удалено');
        }
        return redirect()->back()->with('error','Ошибка');
    }


}
