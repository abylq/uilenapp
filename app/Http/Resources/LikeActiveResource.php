<?php

namespace App\Http\Resources;

use App\Models\Matching;
use App\Models\UserTariff;
use Illuminate\Http\Resources\Json\JsonResource;

class LikeActiveResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $userLikes = Matching::where('to_user_id', auth()->id())->whereNull('to_status');
        $userTariff = UserTariff::where('user_id', auth()->id())->where('activated', 1);
        $arr = "";
        if ($userLikes->exists()) {
            $arr= WhoLikeUserResource::collection($userLikes->get());
        }else {
            $arr = [];
        }
        return [
            'likes'   => $arr,
            'tariff'  => $userTariff->exists() ? new MyTariffResource($userTariff->first()) : null 
        ];
    }
}
