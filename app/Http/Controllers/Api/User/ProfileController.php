<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\CreateQuestionNaireRequest;
use App\Http\Requests\EditProfileRequest;
use App\Http\Resources\UserDetailResource;
use App\Http\Resources\UserResource;
use App\Http\Traits\ImageUploader;
use App\Models\User;
use App\Models\UserAbout;
use App\Models\UserDetail;
use App\Models\UserImage;
use App\Models\UserInterest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{

    /**
     * @return JsonResponse
     */
    public function profile(): JsonResponse
    {
        return response()->json([
            'user' => new UserResource(auth()->user())
        ], Response::HTTP_OK);
    }

    /**
     * @param CreateQuestionNaireRequest $request
     * @return JsonResponse
     */
    public function questionNaire(CreateQuestionNaireRequest $request): JsonResponse
    {
        DB::beginTransaction();
        try {
            $userDetail = UserDetail::where('user_id', $request->get('user_id'));
            $userAbout = UserAbout::where('user_id', $request->get('user_id'));
            if ($userDetail->exists()) {

                $userDetail->update([
                    'city_id'        => $request->get('city_id'),
                    'date_of_birth'  => $request->get('date_of_birth'),
                    'gener_id'       => $request->get('gener_id'),
                    'sex'            => $request->get('sex'),
                    'me_status'      => $request->get('me_status'),
                    'marital_status' => $request->get('marital_status'),
                    'about'          => $request->get('about'),
                    'k_mal'          => $request->get('k_mal'),
                    'bad_habits'     => $request->get('bad_habits'),
                    'zodiac'         => $request->get('zodiac'),
                    'height'         => $request->get('height'),
                    'weight'         => $request->get('weight'),
                ]);
            } else {
                UserDetail::create($request->validated());

            }
            if ($userAbout->exists()) {
                $userAbout->update([
                    'about'          => $request->get('about'),
                    'salary'         => $request->get('salary'),
                ]);
            }else {
                UserAbout::create([
                    'user_id'        => auth()->id(),
                    'about'          => $request->get('about'),
                    'salary'         => $request->get('salary'),
                ]);
            }
            if ($request->interests) {

                $check = UserInterest::where('user_id', auth()->id());
                if ($check->exists()) {
                    $check->delete();
                    foreach ($request->interests as $key => $value) {
                        UserInterest::create([
                            'user_id'       => auth()->id(),
                            'interest_id'   => $value
                        ]);
                    }
                }else {
                    foreach ($request->interests as $key => $value) {
                        UserInterest::create([
                            'user_id'       => auth()->id(),
                            'interest_id'   => $value
                        ]);
                    }
                }

            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json([
                'message' => 'Unable to create user detail' . $th
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'message' => 'Анкета успешно обновлено'
        ], Response::HTTP_OK);
    }

    public function actionQuestionNaire(Request $request)
    {
        $request->validate([
            'status' => 'required|in:active,inactive'
        ]);

        $user = User::find(auth()->id());
        $user->questionnaire_status = $request->status;
        $user->save();

        return response()->json([
            'message' => 'Успешно обновлено'
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updateAbout(Request $request): JsonResponse
    {
        $request->validate([
            'about' => 'string'
        ]);

        try {
            return (new UserDetail())->setEachDetails($request);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Не удалось обновить'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function setSalary(Request $request): JsonResponse
    {
        $request->validate([
            'salary' => 'integer'
        ]);

        try {
            return (new UserDetail())->setEachDetails($request);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Не удалось обновить'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function changePassword(Request $request): JsonResponse
    {
        $request->validate([
            'password' => 'required'
        ]);

        $user = User::find(auth()->id());
        $user->password = bcrypt($request->password);

        if ($user->save()) {
            return response()->json([
                'message' => 'Успешно обновлено'
            ]);
        }

        return response()->json([
            'message' => 'Пожалуйста повторите еще раз'
        ], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function editPhone(Request $request): JsonResponse
    {
        $request->validate([
            'phone' => 'required'
        ]);
        $user = User::find(auth()->id());
        $user->phone = $request->phone;

        if ($user->save()) {
            return response()->json([
                'message' => 'Успешно обновлено'
            ]);
        }

        return response()->json([
            'message' => 'Пожалуйста повторите еще раз'
        ], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function editProfile(EditProfileRequest $request): JsonResponse
    {
        DB::beginTransaction();
        try {
            if($request->phone) {
                auth()->user()->update([
                    'phone' => $request->phone
                ]);
            }
            if($request->email) {
                auth()->user()->update([
                    'email' => $request->email
                ]);
            }
            if($request->password) {
                auth()->user()->update([
                    'password' =>bcrypt($request->get('password'))
                ]);
            }
            $user = (new User())->editProfile($request);
            DB::commit();
        }catch (\Throwable $throwable) {
            DB::rollback();
            return response()->json([
                'message' => 'Пожалуйста повторите еще раз'.$throwable
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'data' => new UserResource(User::find($user->id))
        ]);
    }

}
