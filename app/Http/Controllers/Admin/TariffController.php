<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tariff;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TariffController extends Controller
{

    /**
     * @return View
     */
    public function index(): View
    {
        return \view('admin.tariff.index', [
            'tariffs' => Tariff::withCount('details')->get()
        ]);
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return \view('admin.tariff.create');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name'  => 'required',
            'price' => 'required|integer',
        ]);
        $tariff = Tariff::create([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
        ]);

        return redirect(route('admin.tariff_details.create', 'id='.$tariff->id));
    }

    /**
     * @param Tariff $tariff
     * @return View
     */
    public function edit(Tariff $tariff): View
    {
        return \view('admin.tariff.edit',[
            'tariff' => $tariff
        ]);
    }

    /**
     * @param Tariff $tariff
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Tariff $tariff, Request $request): RedirectResponse
    {
        if ($tariff) {
            $tariff->update([
                'name' => $request->name,
                'price' => $request->price,
                'description' => $request->description,
            ]);
            return redirect()->back();
        }
        return redirect()->back();
    }

    /**
     * @param Tariff $tariff
     * @return RedirectResponse
     */
    public function destroy(Tariff $tariff): RedirectResponse
    {
        // TODO
    }



}
