@extends('admin.layouts.admin')
@section('title','Интересы')
@section('topRightContent')
    <a href="{{ route('admin.interests.create') }}" class="btn btn-primary float-sm-right">Добавить</a>
@endsection
@section('content')
    <div class="card">
        <div class="card-body p-0">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Название</th>

                    <th >действие</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($interests as $item)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $item->title }}</td>
                            <td >
                                <a href="{{ route('admin.interests.edit', $item->id) }}" class="btn btn-warning">
                                    <ion-icon name="pencil-outline"></ion-icon>
                                </a>

                                <a href="{{ route('admin.interests.destroy', $item->id) }}" onclick="return confirm('Вы уверены ?')" class="btn btn-danger">
                                    <ion-icon name="trash-outline"></ion-icon>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>


        </div>
    </div>
@endsection
