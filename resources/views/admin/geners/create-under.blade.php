@extends('admin.layouts.admin')

@section('title','Добавить ')

@section('content')
    <div class="card">
        <div class="card-body">

            <form action="{{ route('admin.geners.store') }}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" required class="form-control">
                </div>
                <div class="form-group">
                    @if(request()->create == 'under-geners')
                        <input type="hidden" name="under_geners" value="1">
                        <label for="name">Выберите ру</label>
                        <select name="geners" class="form-control">
                            @foreach($geners as $item)
                                <option value="{{ $item->id }}"> {{ $item->name }}</option>
                            @endforeach
                        </select>
                    @else
                        <label for="name">Выберите жузы</label>
                        <select name="zhuz" class="form-control">
                            <option value="ulu">Улы жуз</option>
                            <option value="orta">Орта жуз</option>
                            <option value="kishi">Кышы жуз</option>
                            <option value="no">Нет</option>
                        </select>
                    @endif
                </div>
                <div class="form-group">
                    <button class="btn btn-success">Создать</button>
                </div>
            </form>
        </div>
    </div>
@endsection
