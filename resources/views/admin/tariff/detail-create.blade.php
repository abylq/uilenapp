@extends('admin.layouts.admin')

@section('title','Добавить деталь тарифа')

@section('content')
    <div class="card">
        <div class="card-body">

            <form action="{{ route('admin.tariff_details.store') }}" enctype="multipart/form-data" method="post">
                @csrf
                <input type="hidden" name="id" value="{{ request()->id }}">
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text"  required class="form-control" disabled value="{{ $tariff->name }}">
                </div>
                <div class="form-group">
                    <label for="price">Цена</label>
                    <input type="number" min="1" disabled value="{{ $tariff->price }}"required class="form-control">
                </div>
                @for($i = 1; $i < 4; $i++)
                    <div class="form-group">
                        <label for="name">Деталь #{{ $i }} {{ $i == 1 ? 'Обязательное поле' : 'Не Обязательное поле' }}</label>
                        <input type="text"  name="detail[{{ $i }}]" {{ $i == 1 ? 'required' : '' }} class="form-control" placeholder="Пример: Без ограничение лайков">
                    </div>
                @endfor



                <div class="form-group">
                    <button class="btn btn-success">Создать</button>
                </div>
            </form>
        </div>
    </div>
@endsection
