<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotificationSetting extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function getStatus($userId = null)
    {
        $notification = self::where('user_id', $userId ?? auth()->id());
        if($notification->exists()) {
            if($notification->first()->status == 1) {
                return true;
            }
        }
        return false;
    }
}
