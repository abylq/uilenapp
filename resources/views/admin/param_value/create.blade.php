@extends('admin.layouts.admin')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{route('admin.paramValue.store')}}" method="post">
                @csrf
                <input type="hidden" name="param_id" value="{{$param_id}}">
                <div class="form-group">
                    <input type="text" name="value" class="form-control" placeholder="название" required>
                </div>
                <button class="btn btn-primary">добавить</button>
            </form>
        </div>
    </div>
@endsection
