<?php

namespace App\Http\Controllers\Admin\Analytics;

use App\Exports\UserExport;
use App\Http\Controllers\Controller;
use App\Models\Reaction;
use App\Models\User;
use App\Models\UserReaction;
use denis660\Centrifugo\Centrifugo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class UsersController extends Controller
{


    public function mainPage(Centrifugo $centrifugo)
    {
        $reactions = Reaction::select(DB::raw('reactions.*, count(*) as `aggregate`'))
            ->join('user_reactions', 'reactions.id', '=', 'user_reactions.reaction_id')
            ->groupBy('reaction_id')
            ->orderBy('aggregate', 'desc')
            ->get();

        return view('admin.main',[
            'reactions' => $reactions,
        ]);
    }
    /**
     * @param Request $request
     */
    public function report(Request $request)
    {
        return Excel::download(new UserExport($request), date('Y-m-d').'_report.xlsx');
    }

}
