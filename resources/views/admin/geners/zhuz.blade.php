@extends('admin.layouts.admin')
@section('title','Жузы')
@section('topRightContent')
    <a href="{{ route('admin.geners.create') }}" class="btn btn-primary float-sm-right">Добавить </a>

    <a href="{{ route('admin.geners.create', 'create=under-geners') }}" class="btn btn-success float-sm-right mr-1">Добавить под ру</a>
@endsection
@section('content')
    <div class="card">
        <div class="card-body p-0">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Название</th>
                    <th>Ру</th>
                </tr>
                </thead>
                <tbody>
                @if($zhuz->count() > 0)
                    @foreach($zhuz as $item)
                        <tr>
                            <td>{{ $loop->index +1 }}</td>
                            <td><a href="{{ route('admin.geners.list', $item->id) }}">{{ $item->name }}</a></td>
                            <td>{{ $item->geners_count }}</td>
                        </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>


        </div>
    </div>
@endsection
