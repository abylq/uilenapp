<?php

 
use App\Models\Conversation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Auth\RegisterController;
use App\Http\Controllers\Api\Auth\VerifyController;
use App\Http\Controllers\Api\Auth\LogoutController;
use App\Http\Controllers\Api\User\ProfileController;
use App\Http\Controllers\Api\User\InterestController;
use App\Http\Controllers\Api\User\ImageController;
use App\Http\Controllers\Api\User\ChatController;
use App\Http\Controllers\Api\User\LikeController;
use App\Http\Controllers\Api\User\MatchController;
use App\Http\Controllers\Api\CentrifugoController;
use App\Http\Controllers\Api\User\LikeActivesController;
use App\Http\Controllers\Api\User\SearchController;
use App\Http\Controllers\Api\Auth\ResetPasswordController;
use App\Http\Controllers\Api\User\UsersController;
use App\Http\Controllers\Api\User\GenerController;
use App\Http\Controllers\Api\User\UserTariffController;
use App\Http\Controllers\Api\User\LocationController;
use App\Http\Controllers\NotificationStateController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware("localization")->group(function () {
Route::prefix('auth')->group(function () {
    Route::post('/login', LoginController::class);
    Route::post('/register', RegisterController::class);
    Route::post('/sendcode', [VerifyController::class, 'sendCode']);
    Route::post('/sendcodeToNumber', [VerifyController::class, 'sendCodeToNewNumber']);
    Route::post('/verifyphone/{user?}', [VerifyController::class, 'verifyPhone']);
    Route::post('/resetpassword', ResetPasswordController::class)->middleware('auth:sanctum');
});
Route::get('cities',[\App\Http\Controllers\CityController::class, 'index']);
Route::get('regions', [\App\Http\Controllers\CityController::class, 'regions']);
Route::get('region/{region}', [\App\Http\Controllers\CityController::class, 'getCityByRegion']);
Route::get('notification/state/onesignal', [NotificationStateController::class ,'state'])->middleware('auth:sanctum');
Route::get('clearConversation', function () {
    Conversation::query()->delete();
});
Route::group(['prefix' => 'v1','middleware'=> ['auth:sanctum']], function () {
    Route::get('getUser/{user}', [UsersController::class, 'getUser']);
    Route::post('location', [LocationController::class, 'store']);
    Route::post('updateNotification', [LikeController::class,'updateSignal']);
    Route::get('updateChannel',[LikeController::class,'getNotification']);
    Route::post('devicetoken',[UsersController::class,'saveDeviceToken']);
    Route::post('notification/settings', [UsersController::class, 'notificationSettings']);
    Route::post('location/set-distance', [LocationController::class, 'setDistance']);
    Route::group(['prefix' => 'profile'], function () {
        Route::get('/',[ProfileController::class,'profile']);
        Route::post('editPhone', [ProfileController::class, 'editPhone']);
        Route::post('editPassword', [ProfileController::class, 'changePassword']);
        Route::post('editProfile', [ProfileController::class, 'editProfile']);
        Route::post('upload-images',[ImageController::class,'uploadImages']);
        Route::post('avatar/{image}',[ImageController::class,'setAvatar']);
        Route::get('remove-image/{image}', [ImageController::class,'destroy']);
        Route::get('my-images', [ImageController::class, 'myImages']);
        Route::post('questionNaire',[ProfileController::class,'questionNaire']);
        Route::post('about',[ProfileController::class,'updateAbout']);
        Route::post('salary',[ProfileController::class,'setSalary']);
        Route::get('interests',[InterestController::class,'index'])->withoutMiddleware('auth:sanctum');
        Route::post('interests/store/{interest}',[InterestController::class,'store']);
        Route::post('interests/destroy/{interest}',[InterestController::class,'destroy']);
        Route::post('question_naire_status', [ProfileController::class, 'actionQuestionNaire']);
        Route::post('show_gender',[UsersController::class,'showGender']);
    });

    Route::group(['prefix' => 'chat'], function () {
        Route::get('chat-list', [ChatController::class, 'listOfChat']);
        Route::get('is_read/{user}', [ChatController::class, 'isRead']);
        Route::get('is_read_message/{message}', [ChatController::class, 'isReadMessage']);
        Route::post('messages',[ChatController::class,'getMessages']);
        Route::post('sendmessage',[ChatController::class,'sendMessage']);
        Route::post('messenger/gentoken',CentrifugoController::class)->name('messenger.gentoken');
    });

    Route::group(['prefix' => 'like'], function () {
        Route::get('/list',[UsersController::class, 'index']);

        Route::post('/',[LikeController::class, 'like']);
        Route::get('match',[MatchController::class, 'match']);
        Route::get('wholike', [LikeActivesController::class,'index']);
        Route::any('fast_reaction',[UsersController::class, 'fastReaction']);
    });

    Route::group(['prefix' => 'search'], function () {
        Route::post('/', [SearchController::class,'search']);
        Route::get('recently',[SearchController::class,'getRecently']);
        Route::post('recently',[SearchController::class,'postRecently']);
        Route::delete('recently/{user}',[SearchController::class, 'deleteRecently']);
    });

    Route::post('/logout', LogoutController::class);
    Route::apiResource('geners', GenerController::class)->withoutMiddleware('auth:sanctum');

    Route::group(['prefix' => 'tariff'], function () {
        Route::get('/', [UserTariffController::class, 'index']);
        Route::post('/store/{tariff}', [UserTariffController::class, 'store']);
        Route::get('my',[UserTariffController::class,'myTariff']);
        Route::get('cancel/{tariff}', [UserTariffController::class, 'cancelTariff']);
    });

});
});