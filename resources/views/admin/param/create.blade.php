@extends('admin.layouts.admin')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{route('admin.param.store')}}" method="post">
                @csrf
                <input type="hidden" name="cat_id" value="{{$cat_id}}">
                <div class="form-group">
                    <input type="text" name="name" class="form-control" placeholder="название" required>
                </div>
                <button class="btn btn-primary">добавить</button>
            </form>
        </div>
    </div>
@endsection
