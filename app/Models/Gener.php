<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gener extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    /**
     * @return HasMany
     */
    public function geners(): HasMany
    {
        return $this->hasMany(Gener::class, 'parent_id');
    }

    public function byZhuz(): HasMany
    {
        return $this->hasMany(Gener::class, 'zhuz');
    }
}
