<?php

namespace App\Http\Controllers;

use App\Models\NotificationSetting;


class NotificationStateController extends Controller
{
    
    public function state()
    {
        return response()->json([
            'info' => [
                'status' => NotificationSetting::getStatus(),
                'push'   => NotificationSetting::getStatus() ? 'success' : 'failed'
            ] 
        ]);
    }
}
