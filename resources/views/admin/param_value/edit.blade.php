@extends('admin.layouts.admin')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{route('admin.param.update')}}" method="post">
                @csrf
                <input type="hidden" name="id" value="{{$param->id}}">
                <div class="form-group">
                    <input type="text" name="name" value="{{$param->name}}" class="form-control" placeholder="название" required>
                </div>
                <button class="btn btn-primary">добавить</button>
            </form>
        </div>
    </div>
@endsection
