@extends('admin.layouts.admin')

@section('title','Добавить ')

@section('content')
    <div class="card">
        <div class="card-body">

            <form action="{{ route('admin.countries.store-region') }}" enctype="multipart/form-data" method="post">
                @csrf

                <div class="form-group">
                    <label for="name">Страна</label>
                    <select name="country_id" class="form-control">
                        @foreach($countries as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" required class="form-control">
                </div>

                <div class="form-group">
                    <button class="btn btn-success">Создать</button>
                </div>
            </form>
        </div>
    </div>
@endsection
