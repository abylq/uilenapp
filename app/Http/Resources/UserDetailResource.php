<?php

namespace App\Http\Resources;

use App\Models\Gener;
use Illuminate\Http\Resources\Json\JsonResource;

class UserDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'             => $this->id,
            'city_id'        => new CityResource($this->city),
            'date_of_birth'  => $this->date_of_birth,
            'gener_id'       => Gener::find($this->gener_id),
            'sex'            => $this->sex,
            'me_status'      => $this->me_status,
            'marital_status' => $this->marital_status,
            'kalyn_male'     => $this->k_mal,
            'bad_habits'     => $this->bad_habits,
            'zodiac'         => $this->zodiac,
            'height'         => $this->height,
            'weight'         => $this->weight,

        ];
    }
}
