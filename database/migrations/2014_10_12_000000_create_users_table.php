<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->string('phone')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('phone_verified')->nullable();
            $table->string('password');
            $table->enum('sex',[
                'male','female','unisex'
            ])->nullable();
            $table->enum('questionnaire_status',[
                'active','inactive'
            ])->default('active');
            $table->enum('role', [
                'user', 'admin'
            ])->default('user');
            $table->tinyInteger('ban_status')->default(2);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
