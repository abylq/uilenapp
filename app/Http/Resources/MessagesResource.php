<?php

namespace App\Http\Resources;

use App\Models\Conversation;
use App\Models\Reaction;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class MessagesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $reaction = Reaction::where('id', $this->reaction_id);
        $reactionImage = null;
        if ($reaction->exists()) {
            $reactionImage = $reaction->first()->image;
        }
        return [
            'id'            => $this->id,
            'sender_id'     => $this->sender_id,
            'receiver_id'   => $this->receiver_id,
            'message'       => $this->message == "null" ? null : $this->message,
            'file'          => $this->file,
            'reaction'      => $reactionImage,
            'is_read'       => $this->is_read,
            'created_at'    => $this->created_at->format('H:i')

        ];
    }


}
