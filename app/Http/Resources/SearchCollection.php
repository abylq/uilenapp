<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SearchCollection extends ResourceCollection
{

    /**
     * @var PaginationResource
     */
    protected PaginateSearchResource $pagination;

    public function __construct($resource)
    {
        $this->pagination = new PaginateSearchResource($resource);
        parent::__construct($resource);
    }
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'data' => SearchResource::collection($this->collection),
            'pagination' => $this->pagination,
        ];
    }
}
