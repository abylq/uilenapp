<?php

namespace App\Http\Services\Search;
use App\Models\UserLocation;
use Illuminate\Support\Facades\DB;

class SearchUsers implements SearchInterface
{

    private $filterData;
    private $query;

    public function __construct(SearchInterface $filterData)
    {
        $this->query = $filterData->getQuery();
        $this->filterData = $filterData->getFilterData();
    }

    public function getQuery()
    {

        if($this->filterData['name']) {
            $this->query = $this->query->where('users.name', 'LIKE', "%{$this->filterData['name']}%");
        }

        if ($this->filterData['city_id']) {
            $this->query = $this->query->where('user_details.city_id',$this->filterData['city_id']);
        }

        if ($this->filterData['age']) {
            $this->query = $this->query->whereRaw(
                DB::raw('DATEDIFF(CURRENT_DATE, date_of_birth) >= ('.$this->filterData['age'][1].' * 365.25) AND DATEDIFF(CURRENT_DATE, date_of_birth) <= ('.$this->filterData['age'][2].' * 365.25)')
            );
        }

        if($this->filterData['sex']) {
            $this->query = $this->query->where('user_details.sex', 'LIKE', "{$this->filterData['sex']}");
        }
        if($this->filterData['gener_id']) {
            $this->query = $this->query->where('user_details.gener_id', $this->filterData['gener_id']);
        }

        if($this->filterData['marital_status']) {
            $this->query = $this->query->where('user_details.marital_status', 'LIKE', "{$this->filterData['marital_status']}");
        }

        if ($this->filterData['distance']) {
            /** 
            $user = UserLocation::where('user_id', auth()->id());
            if($user->exists()) {
                $raw = DB::raw('ROUND ( ( 6371 * acos( cos( radians('.$user->first()->latitude.') ) * cos( radians( user_locations.latitude ) ) * cos( radians( user_locations.longitude ) - radians('.$user->first()->longitude.') ) + sin( radians('.$user->first()->latitude.') ) * sin( radians( user_locations.latitude ) ) ) ) ) AS rDistance');
                return $this->query->addSelect($raw)->orderBy( 'rDistance', 'DESC' )->having('rDistance', '<=', $this->filterData['distance']);
            }*/
            
        }

        if ($this->filterData['interest_id']) {
            $this->query = $this->query->join('user_interests', function ($join) {
                $join->on('user_interests.user_id','=', 'users.id');
            })->whereIn('user_interests.interest_id', $this->filterData['interest_id']);
        }

        $minMaxArr = [
            'salary','k_mal','weight','height'
        ];
        foreach($minMaxArr as $value) {
            if ($this->filterData[$value]) {
                $this->query = $this->query->where('user_details.'.$value,'>=', (int)$this->filterData[$value][1])
                    ->where('user_details.'.$value,'<=', (int)$this->filterData[$value][2]);
            }
        }


        if($this->filterData['zodiac']) {
            $this->query = $this->query->where('user_details.zodiac', 'LIKE', "{$this->filterData['zodiac']}");
        }

        return $this->query;
    }

    public function getFilterData()
    {

    }
}
