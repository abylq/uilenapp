<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Country;
use App\Models\Region;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use PHPUnit\Framework\Constraint\Count;

class CountryController extends Controller
{

    /**
     * @return View
     */
    public function listOfCountries(): View
    {
        $countries = Country::withCount('regions')->get();

        return \view('admin.countries.index', [
            'countries' => $countries
        ]);
    }

    /**
     * @return View
     */
    public function createCountry(): View
    {
        return \view('admin.countries.create');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function postCountry(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => 'required'
        ]);
        Country::create([
            'name' => $request->name,
        ]);

        return redirect()->back()->with('success','Успешно добавлено');
    }

    /**
     * @return View
     */
    public function listOfCities(Region $region): View
    {
        $cities = City::where('region_id', $region->id)->get();
        return \view('admin.countries.index-city', [
            'cities' => $cities,
            'region' => $region
        ]);
    }

    /**
     * @return View
     */
    public function createCity(): View
    {
        return \view('admin.countries.create-city', [
            'regions' => Region::all()
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function postCity(Request $request): RedirectResponse
    {
        $request->validate([
            'region_id' => 'required|exists:regions,id',
            'name'      => 'required'
        ]);

         City::create([
            'region_id' => $request->region_id,
            'name'      => $request->name,
        ]);


        return redirect()->back()->with('success','Успешно добавлено');
    }

    /**
     * @param Country $country
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|void
     */
    public function editCountry(Country $country, Request $request)
    {
        if ($request->isMethod('POST')) {
            $request->validate([
                'name' => 'required'
            ]);
            if ($country) {
                $country->update([
                    'name' => $request->name
                ]);
                return redirect()->back()->with('success');
            }
        }
        return \view('admin.countries.edit', [
            'country' => $country
        ]);
    }

    /**
     * @param City $city
     * @param Request $request
     */
    public function editCity(City $city, Request $request)
    {

        if ($request->isMethod('POST')) {
            $request->validate([
                'name' => 'required',
                'region_id' => 'required'
            ]);

            if ($city) {
                $city->update([
                    'name'      => $request->name,
                    'region_id' => $request->region_id,
                ]);
            }
        }

        return \view('admin.countries.edit-city', [
            'city' => $city,
            'regions' => Region::all(),
        ]);
    }

    public function createRegion()
    {
        return \view('admin.countries.create-region', [
            'countries' => Country::all()
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function postRegion(Request $request): RedirectResponse
    {
        $request->validate([
            'country_id' => 'required|exists:countries,id',
            'name' => 'required'
        ]);

        Region::create([
            'country_id' => $request->country_id,
            'name' => $request->name,
        ]);

        return redirect()->back()->with('success','Успешно добавлено');
    }

    /**
     * @return View
     */
    public function listOfRegion(Country $country): View
    {
        $regions = Region::where('country_id', $country->id)->withCount('cities')->get();
        return \view('admin.countries.index-region', [
            'regions' => $regions,
            'country' => $country
        ]);
    }

    /**
     * @param City $city
     * @param Request $request
     */
    public function editRegion(Region $region, Request $request)
    {

        if ($request->isMethod('POST')) {
            $request->validate([
                'name' => 'required',
                'country_id' => 'required|exists:countries,id'
            ]);

            if ($region) {
                $region->update([
                    'name' => $request->name,
                    'country_id' => $request->country_id,
                ]);
            }
        }

        return \view('admin.countries.edit-region', [
            'region' => $region,
            'countries' => Country::all(),
        ]);
    }

    public function remove(Request $request)
    {
        switch ($request->page)
        {
            case 'country':
                    $country = Country::find($request->id);
                    $country->delete();
                break;
            case 'region':
                $region = Region::find($request->id);
                $region->delete();
                break;
            case 'city':
                $city = City::find($request->id);
                $city->delete();
                break;
        }

        return redirect()->back();
    }
}
