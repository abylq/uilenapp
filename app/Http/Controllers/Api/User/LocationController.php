<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Models\UserLocation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LocationController extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'lat'  => 'required',
            'long' => 'required'
        ]);

        $user = UserLocation::where('user_id', auth()->id());

        if ($user->exists()) {
            $user->update([
                'user_id'   => auth()->id(),
                'latitude'  => $request->get('lat'),
                'longitude' => $request->get('long'),
            ]);

            return response()->json([
                'message' => 'Updated'
            ]);
        }

        UserLocation::create([
            'user_id'   => auth()->id(),
            'latitude'  => $request->get('lat'),
            'longitude' => $request->get('long'),
        ]);

        return response()->json([
            'message' => 'Успешно создана'
        ]);
    }

    public function setDistance(Request $request)
    {
        $request->validate([
            'distance' => 'required|integer'
        ]);

        $user = UserLocation::where('user_id', auth()->id());
        if ($user->exists()) {
            $user->update(['distance' => $request->distance]);

            return response()->json([
                'message' => 'Успешно добавлено'
            ]);
        }

        return response()->json([
            'message' => 'Пользователь не найден сначала нужно добавить latitude и longitude'
        ], Response::HTTP_BAD_REQUEST);
    }
}
