<?php

namespace App\Http\Resources;

use App\Models\Message;
use Illuminate\Http\Resources\Json\JsonResource;

class UserReceiverResource extends JsonResource
{


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'images'  => UserImageResource::collection($this->images),
            'unread_message' => Message::where(fn($q) => $q
                ->where('receiver_id', $this->id)
                ->where('sender_id', auth()->id())
                ->where('is_read', 0)
            )->orWhere(fn($q) => $q
                ->where('receiver_id', auth()->id())
                ->where('sender_id', $this->id)
                ->where('is_read', 0))
                ->count()
        ];
    }
}
