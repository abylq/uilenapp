@extends('admin.layouts.admin')

@section('title','Добавить ')

@section('content')
    <div class="card">
        <div class="card-body">

            <form action="{{ route('admin.geners.store') }}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" required class="form-control">
                </div>
                <div class="form-group">
                    <label for="name">Выберите жузы</label>
                    <select name="zhuz" class="form-control">
                        <option value="1">Улы жуз</option>
                        <option value="2">Орта жуз</option>
                        <option value="3">Кышы жуз</option>
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-success">Создать</button>
                </div>
            </form>
        </div>
    </div>
@endsection
