<?php

namespace App\Http\Services\Search;

interface SearchInterface
{
    public function getQuery();

    public function getFilterData();
}
