<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class TariffExport  implements FromView, WithColumnWidths,WithStyles
{
    private object $request;
    private string $options;

    /**
     * @param object $request
     * @param string $options
     */
    public function __construct(object $request, string $options)
    {
        $this->request = $request;
        $this->options = $options;
    }

    /**
     * @return View
     */
    public function view(): View
    {
        switch ($this->options) {
            case 'month':
                break;
            case 'week':
                break;
            case 'day':
                break;
        }
    }

    public function columnWidths(): array
    {
        // TODO: Implement columnWidths() method.
    }

    public function styles(Worksheet $sheet)
    {
        // TODO: Implement styles() method.
    }
}
