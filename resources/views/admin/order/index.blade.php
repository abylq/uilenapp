@extends('admin.layouts.admin')
@section('title','Заказы')
@section('content')
    <div class="card">
        <div class="card-body p-0">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Клиент</th>
                    <th>Номер</th>
                    <th>статус</th>
                    <th>действие</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td>{{$order->id}}</td>
                        <td>{{$order->name}}</td>
                        <td>{{$order->phone}}</td>
                        <td>{{$order->status}}</td>
                        <td >
                            <a href="{{route('admin.order.show',$order->id)}}" class="btn btn-primary">
                                <ion-icon name="open-outline"></ion-icon>
                            </a>
                            <a href="{{route('admin.order.destroy',$order->id)}}" onclick="return confirm('Вы уверены ?')" class="btn btn-danger">
                                <ion-icon name="trash-outline"></ion-icon>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>


        </div>
    </div>
@endsection
