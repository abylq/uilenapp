@extends('admin.layouts.admin')
@section('title','Тарифные планы')
@section('topRightContent')
    <a href="{{ route('admin.tariffs.create') }}" class="btn btn-primary float-sm-right">Новый тариф</a>
@endsection
@section('content')
    <div class="card">
        <div class="card-body p-0">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>названия</th>
                    <th>цена</th>
                    <th>Тип</th>
                    <th>Детали</th>
                    <th >действие</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($tariffs as $tariff)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $tariff->name }}</td>
                            <td>{{ $tariff->price }}</td>
                            <td>
                                @switch($tariff->type)
                                    @case('wholike')
                                        Кто лайкнул
                                    @break
                                    @case('popular')
                                        Стать популярным
                                    @break
                                    @case('read_message')
                                        Кто прочитал сообщение
                                    @break
                                    @endswitch
                            </td>
                            <td>
                                <a href="{{ route('admin.tariff_details.show', $tariff->id) }}">{{ $tariff->details_count }}</a>
                            </td>
                            <td >
                                <a href="{{ route('admin.tariffs.edit', $tariff->id) }}" class="btn btn-warning">
                                    <ion-icon name="pencil-outline"></ion-icon>
                                </a>

                                <a href="#" onclick="return confirm('Вы уверены ?')" class="btn btn-danger">
                                    <ion-icon name="trash-outline"></ion-icon>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>


        </div>
    </div>
@endsection
