<?php

namespace App\Http\Resources;

use App\Models\Reaction;
use Illuminate\Http\Resources\Json\JsonResource;

class MessageResourceSocket extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $reaction = Reaction::where('id', $this->reaction_id);
        $reactionImage = null;
        if ($reaction->exists()) {
            $reactionImage = $reaction->first()->image;
        }
        return [
            'id'              => $this->id,
            'conversation_id' => $this->conversation_id,
            'sender_id'       => $this->sender_id,
            'receiver_id'     => $this->receiver_id,
            'message'         => $this->message,
            'is_read'         => $this->is_read,
            'date_message'    => $this->date_message,
            'file'            => $this->file,
            'reaction'        => $reactionImage,
            'created_at'      => date('H:i', strtotime($this->created_at)),
            'updated_at'      => date('Y-m-d H:i:s', strtotime($this->updated_at)),
        ];
    }
}
