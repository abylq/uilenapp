<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;

class Tariff extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'price','description'
    ];

    /**
     * @return HasMany
     */
    public function details(): HasMany
    {
        return $this->hasMany(TariffDetail::class);
    }

    public function profit()
    {
        return Tariff::where('created_at')
               ->join('user_tariffs', 'tariffs.id','=', 'user_tariffs.tariff_id')
               ->select('tariffs.name','SUM(tariffs.price)',DB::raw('count(user_tariffs.user_id) as users_count'))
               ->groupBy('tariffs.name');
    }

    /**
     * @return BelongsTo
     */
    public function userTariff(): BelongsTo
    {
        return $this->belongsTo(UserTariff::class);
    }

    /**
     * @return HasMany
     */
    public function prices(): HasMany
    {
        return $this->hasMany(TariffPrice::class);
    }

    /**
     * @return HasMany
     */
    public function images(): HasMany
    {
        return $this->hasMany(ImageTariff::class);
    }
}
