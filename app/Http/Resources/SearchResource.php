<?php

namespace App\Http\Resources;

use App\Models\Conversation;
use App\Models\UserImage;
use Illuminate\Http\Resources\Json\JsonResource;

class SearchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $conversation = Conversation::where(fn($q) => $q
                ->where('receiver_id', $this->id)
                ->where('sender_id', auth()->id()))->
            orWhere(fn($q) => $q
                ->where('receiver_id', auth()->id())
                ->where('sender_id', $this->id));

        return [
            'id' => $this->id,
            'name' => $this->name,
            'avatar' => UserImage::where('user_id', $this->id)->where('avatar',1)->orderBy('id','DESC')->first()->images ?? null,
            'images' => UserImageResource::collection(UserImage::where('user_id', $this->id)->orderBy('avatar','DESC')->get()),
            'date_of_birth'  => $this->date_of_birth,
            'chat' => (bool) $conversation->exists(),
            'rDistance' => $this->rDistance,
        ];
    }
}
