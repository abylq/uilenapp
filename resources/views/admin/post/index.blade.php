@extends('admin.layouts.admin')
@section('title','Посты')
@section('topRightContent')
    <a href="{{route('admin.post.create')}}" class="btn btn-primary float-sm-right">
        Добавить  пост
    </a>
@endsection
@section('content')
    <div class="card">
        <div class="card-body p-0">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>заголовок</th>
                    <th>действие</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td>{{$post->id}}</td>
                        <td>{{$post->title}}</td>
                        <td>
                            <a href="{{route('admin.post.edit',$post->id)}}" class="btn btn-warning">
                                <ion-icon name="pencil-outline"></ion-icon>
                            </a>
                            <a href="{{route('admin.post.destroy',$post->id)}}" onclick="return confirm('Вы уверены ?')" class="btn btn-danger">
                                <ion-icon name="trash-outline"></ion-icon>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>


        </div>
    </div>
@endsection
