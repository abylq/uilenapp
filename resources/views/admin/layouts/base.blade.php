<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{asset('vendor/admin/plugins/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/admin/dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/admin/plugins/summernote/summernote-bs4.min.css')}}">
</head>
<body class="hold-transition @yield('theme')">

@yield('wrapper')

<!-- jQuery -->
<script src="{{asset('vendor/admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('vendor/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('vendor/admin/dist/js/adminlte.min.js')}}"></script>
<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
<script src="{{asset('vendor/admin/plugins/ekko-lightbox/ekko-lightbox.min.js')}}"></script>
<script src="{{asset('vendor/admin/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
    $('#description').summernote()
    $('#terms_of_use').summernote()
    $('#privacy_policy').summernote()
</script>
@stack('js')
</body>
</html>
