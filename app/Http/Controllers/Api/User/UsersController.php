<?php

namespace App\Http\Controllers\Api\User;

use App\Events\MessageSent;
use App\Http\Controllers\Controller;
use App\Http\Resources\MainUserGetResource;
use App\Http\Resources\MessageResourceSocket;
use App\Http\Resources\PaginationResource;
use App\Http\Resources\ReactionResource;
use App\Http\Resources\UpdateMessageList;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserReactionResource;
use App\Http\Resources\UserResource;
use App\Http\Traits\FCMNotification;
use App\Models\ChatList;
use App\Models\Conversation;
use App\Models\Matching;
use App\Models\Message;
use App\Models\NotificationSetting;
use App\Models\Reaction;
use App\Models\User;
use App\Models\UserReaction;
use Carbon\Carbon;
use denis660\Centrifugo\Centrifugo;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UsersController extends Controller
{
    use FCMNotification;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        if (\request()->get('prev_id')) {
            $users = User::where('role', 'user')
                ->where('id', '!=', auth()->id())
                ->whereNotIn('id', User::getUsersWithoutLiked())
                ->whereHas('detail', function ($query) {
                    if (User::sexStatus() != 'unisex') {
                        $query->where('sex', User::sexStatus());
                    }
                })->where('id', \request()->get('prev_id'))->paginate(10);
            return new PaginationResource($users, UserResource::class);

        }
        
        $users = User::where('role', 'user')
            ->leftJoin('user_locations', function($join) {
                $join->on('users.id','=', 'user_locations.user_id');
            })
            ->select('users.id as user_id')
            //->distance()
            ->where('users.id', '!=', auth()->id())
            ->orderBy('users.id','DESC')
            ->whereNotIn('users.id', User::getUsersWithoutLiked())
                ->whereHas('detail', function ($query) {
                if (User::sexStatus() != 'unisex') {
                    $query->where('sex', User::sexStatus());
                }
        })->paginate(10);

        return new PaginationResource($users, MainUserGetResource::class);
        

        /**$users = User::where('role', 'user')
        ->where('id', '!=', auth()->id())
        ->whereNotIn('id', User::getUsersWithoutLiked())
            ->whereHas('detail', function ($query) {
            if (User::sexStatus() != 'unisex') {
                $query->where('sex', User::sexStatus());
            }
        })->orderBy('id','DESC')->paginate(10);
        //var_dump(User::getUsersWithoutLiked());
        return new PaginationResource($users, UserResource::class);*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showGender(Request $request)
    {
        $request->validate([
            'sex' => 'required|in:male,female,unisex'
        ]);

        $user = User::find(auth()->id());
        $user->sex = $request->sex;
        $user->save();

        return response()->json([
            'message' => 'Успешно обновлено'
        ]);

    }

    /**
     * @param Request $request
     * @param Centrifugo $centrifugo
     * @return JsonResponse|HttpException
     */
    public function fastReaction(Request $request, Centrifugo $centrifugo): JsonResponse|HttpException
    {
        if ($request->isMethod('POST')) {
            $request->validate([
                'user_id' => 'required|exists:users,id',
                'reaction_id' => 'required|exists:reactions,id'
            ]);
            try {
                $centrifugo->info();
            } catch (ConnectException $e) {
                throw new HttpException(503);
            }
            try {
                if ($create = UserReaction::create([
                    'user_id' => $request->get('user_id'),
                    'from_user_id' => auth()->id(),
                    'reaction_id' => $request->get('reaction_id')
                ])) {
                    $conversation = (new Conversation())->createConversation(
                        $request->get('user_id'),
                        Carbon::now()->format('Y-m-d')
                    );
                    (new ChatList())->checkChatList($request->get('user_id'));
                    $message = Message::create([
                        'conversation_id' => $conversation->id,
                        'sender_id'       => auth()->user()->id,
                        'receiver_id'     => $request->get('user_id'),
                        'is_read'         => 0,
                        'date_message'    => Carbon::now()->format('Y-m-d'),
                        'reaction_id'     => $create->reaction_id,
                    ]);
                    $messageGet = DB::table('messages')->where('id',$message->id)->first();
                    MessageSent::dispatch($messageGet);
                    $centrifugo->publish('sender_'.auth()->id(), ['message' => response()->json(new MessageResourceSocket($messageGet))->getData()]);
                    $centrifugo->publish('listOfChat_'.auth()->id(), [
                        response()->json(new UpdateMessageList($this->updateChat(auth()->id(),'sender_id')))->getData()
                    ]);
                    $centrifugo->publish('listOfChat_'.$message->receiver_id, [
                        response()->json(new UpdateMessageList($this->updateChat($message->receiver_id,'receiver_id')))->getData()
                    ]);
                    $oneSignal = $this->sendNotificationOneSignal($request->get('user_id'),'Реакция',$messageGet, 'reaction');

                    return response()->json([
                        'message' => 'Успешно отправлено',
                        'data' => new MessageResourceSocket($messageGet),
                        'oneSignal' => $oneSignal,
                    ]);
                }
            } catch (\Exception $exception) {
                return response()->json([
                    'message' => 'Неизвестная ошибка' 
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        $reaction = Reaction::all();

        return response()->json([
            'data' => ReactionResource::collection($reaction)
        ]);
    }

    /**
     * @param User $user
     * @return JsonResponse
     */
    public function getUser(User $user): JsonResponse
    {
        return response()->json([
            'user' => new UserResource($user)
        ], Response::HTTP_OK);
    }

    private function updateChat($id, $typeUserId)
    {
        $message = Message::where([
            $typeUserId => $id
        ])->select($typeUserId)->orderBy('id','DESC')->first();

        return $message;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function saveDeviceToken(Request $request): JsonResponse
    {
        auth()->user()->update([
            'app_client_id' => $request->get('app_client_id')
        ]);

        return response()->json(['Успешно']);
    }
    
    public function notificationSettings(Request $request): JsonResponse
    {
        $request->validate([
            'status' => 'required'
        ]);
        $addSettings = (new User())->setNotificationSettings($request, auth()->id());
        return response()->json($addSettings);
    }

 
}
