<?php

namespace App\Http\Controllers\Admin\Analytics;

use App\Http\Controllers\Controller;
use App\Models\Matching;
use App\Models\Message;
use App\Models\Tariff;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\View\View;

class StaticsController extends Controller
{

    /**
     * @return View
     */
    public function index(): View
    {
        $users = User::where('role', 'user')->count();
        $matchedUsers = Matching::where('matched', 1)->count();
        $tariffs = Tariff::count();
        $messages = Message::count();

    }
}
