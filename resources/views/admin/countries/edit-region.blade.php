@extends('admin.layouts.admin')

@section('title', $region->name)

@section('content')
    <div class="card">
        <div class="card-body">

            <form action="{{ route('admin.countries.update-region', $region->id) }}" enctype="multipart/form-data" method="post">
                @csrf

                <div class="form-group">
                    <label for="name">Страна</label>
                    <select name="country_id" class="form-control">
                        @foreach($countries as $item)
                            <option value="{{ $item->id }}" {{ $region->country_id == $item->id ? 'selected': '' }}>{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" required class="form-control" value="{{ $region->name }}">
                </div>

                <div class="form-group">
                    <button class="btn btn-success">Создать</button>
                </div>
            </form>
        </div>
    </div>
@endsection
