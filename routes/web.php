<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\TariffController;
use App\Http\Controllers\Admin\TariffDetailsController;
use App\Http\Controllers\Admin\InterestController;
use App\Http\Controllers\Admin\GenersController;
use App\Http\Controllers\Admin\CountryController;
use App\Http\Controllers\Admin\Analytics\UsersController as AnalyticsUsersController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[LoginController::class,'index'])->name('login');
Route::get('/login',[LoginController::class,'login']);
Route::post('/login',[LoginController::class,'auth'])->name('auth');

Route::group(['prefix' => 'admin', 'middleware'=> ['roles:admin'],'as' => 'admin.'], function () {
    Route::get('/',[AnalyticsUsersController::class, 'mainPage'])->name('list');
    Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
        Route::get('/',[UsersController::class, 'index'])->name('list');
        Route::get('show/{user}',[UsersController::class, 'show'])->name('show');
        Route::get('/destroy/{user}',[UsersController::class, 'destroy'])->name('destroy');
    });

    Route::resource('tariffs',TariffController::class);
    Route::resource('tariff_details', TariffDetailsController::class);
    Route::resource('interests',  InterestController::class);
    Route::get('interests/destroy/{id}', [InterestController::class,'destroy'])->name('interests.destroy');
    Route::resource('geners', GenersController::class);
    Route::get('geners/list/{zhuz}', [GenersController::class, 'listByZhuz'])->name('geners.list');
    Route::get('delete/{id}', [GenersController::class, 'destroy'])->name('geners.delete');
    Route::group(['prefix' => 'countries', 'as' => 'countries.'], function () {
        Route::get('/',[CountryController::class, 'listOfCountries'])->name('list_of_countries');
        Route::get('create',[CountryController::class, 'createCountry'])->name('create-country');
        Route::post('store-countries', [CountryController::class, 'postCountry'])->name('store-countries');
        Route::any('update-country/{country}', [CountryController::class, 'editCountry'])->name('update-country');

        Route::get('list-cities/{region}',[CountryController::class, 'listOfCities'])->name('list_of_cities');
        Route::get('list-region/{country}',[CountryController::class, 'listOfRegion'])->name('list_of_region');
        Route::get('remove',[CountryController::class, 'remove'])->name('remove');

        Route::get('create-city',[CountryController::class, 'createCity'])->name('create-city');
        Route::get('create-region',[CountryController::class, 'createRegion'])->name('create-region');

        Route::post('store-cities', [CountryController::class, 'postCity'])->name('store-cities');
        Route::any('update-city/{city}', [CountryController::class, 'editCity'])->name('update-city');
        Route::any('update-region/{region}', [CountryController::class, 'editRegion'])->name('update-region');

        Route::post('store-region', [CountryController::class, 'postRegion'])->name('store-region');

    });
});

Route::get('terms', function () {
    echo 'terms';
});

Route::get('privacy', function () {
    echo 'privacy';
});


