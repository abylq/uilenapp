@extends('admin.layouts.admin')

@section('title',$product->name)

@section('content')

        <div class="card card-solid">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <div class="card-body">
                            <div class="row">
                                @foreach($images as $img)
                                    <div class="col-sm-2">
                                        <a href="{{asset($img)}}" data-toggle="lightbox" data-title="sample 1 - white" data-gallery="gallery">
                                            <img src="{{asset($img)}}" class="img-fluid mb-2" alt="white sample">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <h3 class="my-3">{{$product->name}}</h3>
                        <hr>
                        <div class="row">
                            <a href="{{route('admin.product.edit',$product->id)}}">edit</a>
                        </div>
                        <table class="table table-bordered">
                           @foreach($params as $param)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$param->param}}</td>
                                    <td>{{$param->value}}</td>
                                </tr>
                           @endforeach

                        </table>



                        <div class="bg-gray py-2 px-3 mt-4">
                            <h2 class="mb-0">
                                {{$product->price}} тенге
                            </h2>
                        </div>




                    </div>
                </div>
                <div class="row mt-4">
                    <nav class="w-100">
                        <div class="nav nav-tabs" id="product-tab" role="tablist">
                            <a class="nav-item nav-link active" id="product-desc-tab" data-toggle="tab" href="#product-desc" role="tab" aria-controls="product-desc" aria-selected="true">Описане</a>
                            <a class="nav-item nav-link" id="product-rating-tab" data-toggle="tab" href="#product-rating" role="tab" aria-controls="product-rating" aria-selected="false">Отзывы</a>
                        </div>
                    </nav>
                    <div class="tab-content p-3" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="product-desc" role="tabpanel" aria-labelledby="product-desc-tab">
                         {!! $product->description !!}
                        </div>
                        <div class="tab-pane fade" id="product-rating" role="tabpanel" aria-labelledby="product-rating-tab">
                            @foreach($reviews as $review)
                                <div class="row">
                                    <div class="col-md-1">
                                        <img class="img-circle" src="{{asset($review->avatar)}}" width="50" alt="">
                                    </div>
                                    <div class="col-md-8">
                                        <h4>{{$review->name}}</h4>
                                        <p> {{$review->text}}
                                        </p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
    </div>

@endsection
