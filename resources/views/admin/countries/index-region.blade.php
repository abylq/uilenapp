@extends('admin.layouts.admin')
@section('title','Области')
@section('topRightContent')
    <a href="{{ route('admin.countries.create-region') }}" class="btn btn-primary float-sm-right">Добавить</a>
@endsection
@section('content')
    <div class="card">
        <div class="card-body p-0">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Название</th>
                    <th>Страна</th>
                    <th>Кол-во городов</th>
                    <th >действие</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($regions as $item)

                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td><a href="{{ route('admin.countries.list_of_cities', $item->id) }}">{{ $item->name }}</a></td>
                            <td>{{ $country->name }}</td>
                            <td><a href="{{ route('admin.countries.list_of_cities', $item->id) }}">{{ $item->cities_count }}</a></td>
                            <td >

                                <a href="{{ route('admin.countries.update-region', $item->id) }}" class="btn btn-warning">
                                    <ion-icon name="pencil-outline"></ion-icon>
                                </a>

                                <a href="{{ route('admin.countries.remove','page=region&id='.$item->id) }}" onclick="return confirm('Вы уверены ?')" class="btn btn-danger">
                                    <ion-icon name="trash-outline"></ion-icon>
                                </a>
                            </td>
                        </tr>

                    @endforeach

                </tbody>
            </table>


        </div>
    </div>
@endsection
