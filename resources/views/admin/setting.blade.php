@extends('admin.layouts.admin')

@section('title','Метаданные')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{route('admin.setting.update')}}"  method="post">
                @csrf
                <div class="form-group">
                    <label for="phone">Телефон номер</label>
                    <input type="text" name="phone" class="form-control" value="{{$setting->phone}}">
                </div>
                <div class="form-group">
                    <label for="terms_of_use">Условия использования</label>
                    <textarea name="terms_of_use" id="terms_of_use" class="form-control">{{$setting->terms_of_use}}</textarea>
                </div>
                <div class="form-group">
                    <label for="privacy_policy">политика конфиденциальности</label>
                    <textarea name="privacy_policy" id="privacy_policy" class="form-control">{{$setting->privacy_policy}}</textarea>
                </div>
            </form>
        </div>
    </div>
@endsection
