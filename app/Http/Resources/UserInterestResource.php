<?php

namespace App\Http\Resources;

use App\Models\Interest;
use Illuminate\Http\Resources\Json\JsonResource;

class UserInterestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $interest = Interest::where('id', $this->interest_id)->first();
        return [
            'id'    => $interest->id,
            'title' => $interest->title,
        ];
    }
}
