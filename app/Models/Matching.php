<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Matching extends Model
{
    use HasFactory;

    const LIKE      = 1;
    const DISLIKE   = 0;
    protected $table = 'matches';

    protected $guarded = [];

    /**
     * @return HasMany
     */
    public function userLikes(): HasMany
    {
        return $this->hasMany(User::class, 'id','from_user_id');
    }

    /**
     * @return BelongsTo
     */
    public function toUserId(): BelongsTo
    {
        return $this->belongsTo(User::class, 'to_user_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function fromUserId() : BelongsTo
    {
        return $this->belongsTo(User::class, 'from_user_id', 'id');
    }

    /**
     * @param object $request
     */
    public function likeUser(object $request)
    {
        $create = Matching::create([
            'to_user_id' => $request->get('to_user_id'),
            'from_user_id' => auth()->id(),
            'status' => $request->get('superlike') == 1 ? 1 : $request->get('status'), // LIKE DISLIKE status
            'super_like' => $request->get('superlike') == 1 ? 1 : 0
        ]);

        return $create;
    }


    public function tariff()
    {
        $tariff = Tariff::with(['userTariff' => function($query) {
            $query->where('user_id', auth()->id())
            ->where('activated',1);
        }])->first();

        return $tariff;
    }
}
