<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RegisterController extends Controller
{


    /**
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function __invoke(RegisterRequest $request): JsonResponse
    {
        try {
            $user = User::create([
                'name'     => $request->name,
                'phone'    => $request->phone,
                'password' => bcrypt($request->password),
                'app_client_id' => $request->app_client_id
            ]);
            return response()->json([
                'message' => 'Успешно создана',
                'data' => new UserResource($user),
            ],Response::HTTP_CREATED);
        }catch (\DomainException $e) {
            return response()->json([
               'message' => 'failed'.$e
            ],Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
