<?php

namespace App\Http\Resources;

use App\Models\Gener;
use Illuminate\Http\Resources\Json\JsonResource;

class GenerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'parents' => GenerParentResource::collection(Gener::where('zhuz_id', $this->id)->where('parent_id',0)->get()),
        ];
    }
}
