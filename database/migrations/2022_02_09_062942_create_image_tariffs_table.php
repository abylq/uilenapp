<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImageTariffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_tariffs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tariff_id')->constrained()->onDelete('cascade');
            $table->string('title');
            $table->string('description');
            $table->text('image');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_tariffs');
    }
}
