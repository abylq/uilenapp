<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserTariff extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function tariff(): BelongsTo
    {
        return $this->belongsTo(Tariff::class,'tariff_id','id');
    }

    /**
     * @return BelongsTo
     */
    public function tariffPrice(): BelongsTo
    {
        return $this->belongsTo(TariffPrice::class, 'price_id', 'id');
    }
}
//DB_PASSWORD=adgjmp96
