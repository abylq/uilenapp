<?php

namespace App\Http\Resources;

use App\Models\Gener;
use Illuminate\Http\Resources\Json\JsonResource;

class GenerParentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'parents' => GenerUnderParentResource::collection(Gener::where('parent_id', $this->id)->get()),
        ];
    }
}
