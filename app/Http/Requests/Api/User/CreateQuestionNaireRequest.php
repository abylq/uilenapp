<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
class CreateQuestionNaireRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id'        => 'exists:cities,id',
            'date_of_birth'  => 'date_format:Y-m-d',
            'gener_id'       => 'exists:geners,id',
            'sex'            => 'in:male,female',
            'me_status'      => 'string',
            'marital_status' => 'string',
            'about'          => 'string',
            'user_id'        => 'required|exists:users,id',
            'k_mal'          => 'string',
            'bad_habits'     => 'string',
            'zodiac'         => 'string',
            'weight'         => 'string',
            'height'         => 'string',
        ];
    }

    

     /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {

        $response = [
            'message' => app()->getLocale() == 'ru' ? 'Неверные данные' : 'Қате',
            'errors' => $validator->errors(),
        ];

        throw new HttpResponseException(response()->json($response, 422));
    }
}
