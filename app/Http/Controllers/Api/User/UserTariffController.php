<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\MyTariffResource;
use App\Http\Resources\TariffResource;
use App\Models\Tariff;
use App\Models\UserTariff;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;

class UserTariffController extends Controller
{


    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $tariff = Tariff::take(1)->first();

        return response()->json([
            'data' => new TariffResource($tariff)
        ]);
    }

    public function store(Request $request, Tariff $tariff)
    {
        $request->validate([
            'activated' => 'required|numeric',
            'end_date' => 'required|numeric',
        ]);
        try {
            if (UserTariff::where(['user_id' => auth()->id(), 'tariff_id' => $tariff->id])->exists()) {
                return response()->json([
                    'message' => 'The user already has a tariff '
                ], Response::HTTP_BAD_REQUEST);
            }
            UserTariff::create([
                'user_id'   => auth()->id(),
                'tariff_id' => $tariff->id,
                'price_id'  => $request->get('price_id'),
                'activated' => $request->activated,
                'end_date'  => Carbon::now()->addMonths($request->end_date)->format('Y-m-d'),
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'Повторите еще раз'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'data' => 'Вы приобрели подписку успешно'
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function myTariff(): JsonResponse
    {
        $userTariff = UserTariff::where('user_id', auth()->id())->where('activated', 1)->first();

        if ($userTariff) {
            return response()->json([
                'data' => new MyTariffResource($userTariff)
            ]); 
        }
        return response()->json([
            'data' => [
                'tariff' => null
            ]
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function cancelTariff(Tariff $tariff): JsonResponse
    {

        try {
            $userTariff = UserTariff::where(['tariff_id' => $tariff->id, 'user_id' => auth()->id()]);
            if ($userTariff->exists()) {
                $userTariff->delete();
            } else {
                return response()->json([
                    'message' => 'Тариф не найден'
                ], Response::HTTP_BAD_REQUEST);
            }
        } catch (\Throwable $throwable) {
            return response()->json([
                'message' => $throwable
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }


        return response()->json([
            'message' => 'Успешно отменено'
        ]);
    }
}
