<?php

namespace App\Http\Controllers\Admin\Analytics;

use App\Http\Controllers\Controller;
use App\Models\UserTariff;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TariffController extends Controller
{


    /**
     * @return View
     */
    public function popularTariff(): View
    {
        $tariff = UserTariff::selectRaw("tariff_id, count(id) as populars")
            ->groupBy('tariff_id')
            ->orderBy('populars','DESC')
            ->get();

        return view('admin.analytics.popular', [
            'tariff' => $tariff
        ]);
    }

    /**
     * @param Request $request
     */
    public function report(Request $request)
    {

    }

    public function profit()
    {
        $date = request()->date;


    }
}
