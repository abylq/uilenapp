<?php

namespace App\Http\Controllers\Api\User;

use App\Events\MessageSent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\CreateMessageRequest;
use App\Http\Requests\Api\User\MessageGetRequest;
use App\Http\Resources\ChatResource;
use App\Http\Resources\ConversationResource;
use App\Http\Resources\MessageCollection;
use App\Http\Resources\MessageList;
use App\Http\Resources\MessageResourceSocket;
use App\Http\Resources\MessagesResource;
use App\Http\Resources\PaginationResource;
use App\Http\Resources\UpdateMessageList;
use App\Http\Resources\UserImageResource;
use App\Http\Traits\FCMNotification;
use App\Http\Traits\ImageUploader;
use App\Models\Chat;
use App\Models\ChatList;
use App\Models\Conversation;
use App\Models\Message;
use App\Models\MessageFile;
use App\Models\User;
use Carbon\Carbon;
use denis660\Centrifugo\Centrifugo;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ChatController extends Controller
{
    use ImageUploader, FCMNotification;
    /**
     * @param MessageGetRequest $request
     * @return ResourceCollection
     */
    public function getMessages(MessageGetRequest $request): ResourceCollection
    {
        $conversation = Conversation::where(fn($q) => $q
            ->where('receiver_id', $request->get('receiver_id'))
            ->where('sender_id', auth()->id()))->
            orWhere(fn($q) => $q
            ->where('receiver_id', auth()->id())
            ->where('sender_id', $request->get('receiver_id')))
            ->with(['messages' => function ($query) {
                    $query->orderBy('id','DESC');
            }])
            ->orderBy('id','DESC')
            ->paginate(10);
       return new PaginationResource($conversation, ConversationResource::class);
    }

    /**
     * @param CreateMessageRequest $request
     * @param Centrifugo $centrifugo
     * @return Response|HttpException
     */
    public function sendMessage(CreateMessageRequest $request, Centrifugo $centrifugo): JsonResponse|HttpException
    {
        try {
            $centrifugo->info();
        } catch (ConnectException $e) {
            throw new HttpException(503);
        }
        $conversation = (new Conversation())->createConversation(
            $request->get('receiver_id'),
            Carbon::now()->format('Y-m-d')
        );
        if ($request->hasFile('file')) {
            $path = $this->upload($request->file('file'),'user_files/');
        }else {
            $path = null;
        }
        $message = Message::create([
            'conversation_id' => $conversation->id,
            'sender_id'       => auth()->user()->id,
            'receiver_id'     => $request->get('receiver_id'),
            'message'         => $request->get('message'),
            'is_read'         => 0,
            'date_message'    => Carbon::now('Asia/Almaty')->format('Y-m-d'),
            'file'            => $path,
            'reaction_id'     => $request->get('reaction_id'),
            'created_at'      => Carbon::now()->timezone('Asia/Almaty'),
            'updated_at'      => Carbon::now()->timezone('Asia/Almaty'),
        ]);
        (new ChatList())->checkChatList($request->get('receiver_id'));
        $messageGet = DB::table('messages')->where('id',$message->id)->first();
        MessageSent::dispatch($messageGet);

        $centrifugo->publish('sender_'.auth()->id(), ['message' => response()->json(new MessageResourceSocket($messageGet))->getData()]);

        $centrifugo->publish('listOfChat_'.auth()->id(), [
            response()->json(new UpdateMessageList($this->updateChat(auth()->id(),'sender_id')))->getData()
        ]);
        $centrifugo->publish('listOfChat_'.$message->receiver_id, [
            response()->json(new UpdateMessageList($this->updateChat($message->receiver_id,'receiver_id')))->getData()
        ]);

        if (Cache::get('message_user_id_'.$message->receiver_id)) {
            Cache::forget('message_user_id_'.$message->receiver_id);
        }
        Cache::put('message_user_id_'.$message->receiver_id, 'chat', now()->addMinutes(43800));
        $centrifugo->publish('signal_'.$message->receiver_id, [
                response()->json($this->getSignal($message->receiver_id))->getData()
        ]);
        $fromTitleMessage = "Новое сообщение от ".auth()->user()->name;
        //$notifcationFCM = $this->sendFcmNotification($message->receiver_id,'Новое сообщение',$message->message);
       //$huaweiNotification = $this->sendNotificationHuawei($message->receiver_id,'Новое сообщение',$messageGet, 'message');
        $oneSignal = $this->sendNotificationOneSignal($message->receiver_id,$fromTitleMessage,$messageGet, 'message');
        return response()->json([
            'data' => new MessageResourceSocket($messageGet),
            //'notificationFCM' => $notifcationFCM,
            //'huaweiNotification' => $huaweiNotification,
            "oneSignal" => $oneSignal,
        ]);
    }


    public function listOfChat()
    {
       $conversation = ChatList::where('sender_id', auth()->id())
           ->orWhere('receiver_id', auth()->id())
           ->orderBy('updated_at','DESC')->paginate(10);

       return new PaginationResource($conversation, MessageList::class);
    }

    /**
     * @param User $user
     * @return JsonResponse
     */
    public function isRead(User $user, Centrifugo $centrifugo): JsonResponse
    {
        if ($user) {
            Message::where(fn($q) => $q
                ->where('receiver_id', $user->id)
                ->where('sender_id', auth()->id())
                ->where('is_read', 0)
            )->orWhere(fn($q) => $q
                ->where('receiver_id', auth()->id())
                ->where('sender_id', $user->id)
                ->where('is_read', 0))
                ->update([
                'is_read' => 1
            ]);
            $this->updateSignal($centrifugo);
            return response()->json([
                'message' => 'Success'
            ]);
        }

        return response()->json([
            'message' => 'Please repeat again'
        ], Response::HTTP_BAD_REQUEST);
    }

    private function updateChat($id, $typeUserId)
    {
        $message = Message::where([
            $typeUserId => $id
        ])->select($typeUserId)->orderBy('id','DESC')->first();

        return $message;
    }

    /**
     * @param Message $message
     * @param Centrifugo $centrifugo
     * @return JsonResponse|HttpException
     */
    public function isReadMessage(Message $message, Centrifugo $centrifugo): JsonResponse|HttpException
    {
        try {
            $centrifugo->info();
        } catch (ConnectException $e) {
            throw new HttpException(503);
        }

        if ($message) {
            $message->update([
                'is_read' => 1
            ]);
            $centrifugo->publish('listOfChat_'.$message->sender_id, [
                response()->json(new UpdateMessageList($this->updateChat($message->sender_id,'sender_id')))->getData()
            ]);
            $centrifugo->publish('listOfChat_'.$message->receiver_id, [
                response()->json(new UpdateMessageList($this->updateChat($message->receiver_id,'receiver_id')))->getData()
            ]);
            $this->updateSignal($centrifugo);
            return response()->json([
                'message' => 'Success'
            ]);
        }

        return response()->json([
            'message' => 'Try again please'
        ], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param Centrifugo $centrifugo
     */
    private function updateSignal(Centrifugo $centrifugo)
    {
        if (Cache::get('message_user_id_'.auth()->id())) {
            Cache::forget('message_user_id_'.auth()->id());

            $centrifugo->publish('signal_'.auth()->id(), [
                response()->json($this->getSignal(auth()->id()))->getData()
            ]);
        }
    }

    /**
     * @return array
     */
    private function getSignal($userId): array
    {
        $message = Message::where('receiver_id', $userId)->where('is_read',0)->exists();
        $data = [
            'chat' => (bool)$message,
            'like' => (bool)Cache::get('like_user_id_'.$userId),
            'matched' => (bool)Cache::get('matched_user_id_'.$userId),
        ];
        return $data;
    }


}
