<?php

namespace App\Http\Controllers;

use App\Http\Resources\CityResource;
use App\Http\Resources\CountryResource;
use App\Http\Resources\RegionResource;
use App\Models\City;
use App\Models\Country;
use App\Models\Region;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CityController extends Controller
{

    public function index()
    {
        $cities = City::all();

        return response()->json([
            'data' => CityResource::collection($cities)
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function regions(): JsonResponse
    {
        $region = Region::all();

        return response()->json([
            'data' => RegionResource::collection($region)
        ]);
    }

    public function getCityByRegion(Region $region): JsonResponse
    {

        return response()->json([
            'data' => new RegionResource($region)
        ]);
    }
}
