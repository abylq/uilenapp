<?php

namespace App\Http\Resources;

use App\Models\User;
use App\Models\UserImage;
use App\Models\UserTariff;
use Illuminate\Http\Resources\Json\JsonResource;

class MainUserGetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $user = User::find($this->user_id);
        $tariff = UserTariff::where('user_id', auth()->id());
        $tarifId = null;
        if ($tariff->exists()) {
            $tarifId = $tariff->first()->tariff_id;
        }
        return [
            'id' => $user->id,
            'name' => $user->name,
            'rDistance' => $this->rDistance,
            'phone' => $user->phone,
            'email' => $user->email,
            'avatar' => UserImage::where('user_id', $user->id)->where('avatar',1)->orderBy('id','DESC')->first()->images ?? null,
            'interests' => UserInterestResource::collection($user->interests),
            'details' => new UserDetailResource($user->detail),
            'images'  => UserImageResource::collection($user->images),
            'location' => new UserLocationResource($user->location),
            'questionnaire_status' => $user->questionnaire_status,
            'about'          => new AboutResource($user->about),
            'users_reaction' => MyReactionResource::collection($user->reactions),
            'show_gender' => $user->sex,
            'tarif_id' => $tarifId,
            'device_token' => $user->device_token,
            'app_client_id' => $user->app_client_id,
            'notificationSettings' => $user->notificationSettings,
        ];
    }
}
