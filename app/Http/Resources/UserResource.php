<?php

namespace App\Http\Resources;

use App\Models\Conversation;
use App\Models\Matching;
use App\Models\UserImage;
use App\Models\UserTariff;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\LengthAwarePaginator;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $tariff = UserTariff::where('user_id', auth()->id());
        $tarifId = null;
        if ($tariff->exists()) {
            $tarifId = $tariff->first()->tariff_id;
        }
        $conversation = Conversation::where(fn($q) => $q
                ->where('receiver_id', $this->id)
                ->where('sender_id', auth()->id()))->
            orWhere(fn($q) => $q
                ->where('receiver_id', auth()->id())
                ->where('sender_id', $this->id));
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'avatar' => UserImage::where('user_id', $this->id)->where('avatar',1)->orderBy('id','DESC')->first()->images ?? null,
            'interests' => UserInterestResource::collection($this->interests),
            'details' => new UserDetailResource($this->detail),
            'images'  => UserImageResource::collection($this->images),
            'location' => new UserLocationResource($this->location),
            'questionnaire_status' => $this->questionnaire_status,
            'about'          => new AboutResource($this->about),
            'users_reaction' => MyReactionResource::collection($this->reactions),
            'show_gender' => $this->sex,
            'tarif_id' => $tarifId,
            'device_token' => $this->device_token,
            'app_client_id' => $this->app_client_id,
            'notificationSettings' => $this->notificationSettings,
            'chat' =>  (bool) $conversation->exists(),
        ];
    }
}
