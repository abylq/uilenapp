@if ($paginator->lastPage() > 1)
    <ul class="pagination">
        @if($paginator->currentPage() > 1)
            <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
                <a href="{{ $paginator->url($paginator->currentPage() - 1) }}"><ion-icon name="arrow-back-outline"></ion-icon></a>
            </li>
        @endif
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
            </li>
        @endfor
        @if($paginator->currentPage() < $paginator->lastPage())
            <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
                <a href="{{ $paginator->url($paginator->currentPage()+1) }}" ><ion-icon name="arrow-forward-outline"></ion-icon></a>
            </li>
        @endif
    </ul>
@endif
