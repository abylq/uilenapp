@extends('admin.layouts.admin')
@section('title','Страны')
@section('topRightContent')
    <a href="{{ route('admin.countries.create-country') }}" class="btn btn-primary float-sm-right">Добавить</a>
@endsection
@section('content')
    <div class="card">
        <div class="card-body p-0">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Название</th>
                    <th>Кол-во области</th>
                    <th >действие</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($countries as $item)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td><a href="{{ route('admin.countries.list_of_region', $item->id) }}">{{ $item->name }}</a></td>
                            <td><a href="{{ route('admin.countries.list_of_region', $item->id) }}">{{ $item->regions_count }}</a></td>
                            <td >

                                <a href="{{ route('admin.countries.update-country', $item->id) }}" class="btn btn-warning">
                                    <ion-icon name="pencil-outline"></ion-icon>
                                </a>

                                <a href="{{ route('admin.countries.remove','page=country&id='.$item->id) }}" onclick="return confirm('Вы уверены ?')" class="btn btn-danger">
                                    <ion-icon name="trash-outline"></ion-icon>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>


        </div>
    </div>
@endsection
