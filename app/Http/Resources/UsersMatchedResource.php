<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UsersMatchedResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return [
            'id'         => $this->id,
            'status'     => $this->status,
            'matched'    => $this->matched ?? 0,
            'created_at' => $this->created_at,

            'to_user'    => new UserInfoResource($this->toUserId),
            'from_user'  => new UserInfoResource($this->fromUserId),
            'super_like' => $this->super_like,
        ];
    }
}
