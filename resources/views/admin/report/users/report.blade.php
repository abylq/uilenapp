<!DOCTYPE html>
<html>
<head>
    <title>HTML Table Cellpadding</title>
</head>
<body>
<table border="1" cellpadding="5" cellspacing="5">
    <tr>
        <th>#</th>
        <th>ФИО</th>
        <th>Номер телефона</th>
        <th>Город</th>
        <th>Пол</th>
        <th>Дата регистрация</th>
    </tr>
    @if($users->count() > 0)
        @foreach($users as $item)
            <tr>
                <td>{{ $item->index + 1 }}</td>
                <td> {{ $item->name }}</td>
                <td>{{ $item->phone }}</td>
                <td>
                    @foreach($item->detail as $detail)
                        {{ $detail->city['name'] }}
                    @endforeach
                </td>
                <td>{{ $item->sex == 'male' ? 'Мужчина' : 'Женщина' }}</td>
                <td>{{ $item->created_at }}</td>
            </tr>
        @endforeach
    @endif
</table>
</body>
</html>
