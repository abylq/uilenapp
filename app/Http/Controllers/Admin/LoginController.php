<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class LoginController extends Controller
{

    /**
     * @return RedirectResponse
     */
    public function index(): RedirectResponse
    {
        if (!Auth::check()) {
            return redirect('/login');
        }

        return redirect('/'.Auth::user()->role);
    }

    /**
     * @return View
     */
    public function login(): View
    {
        return view('admin.login');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function auth(Request $request): RedirectResponse
    {

        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => 'admin'])) {
            return redirect('/'.Auth::user()->role);
        }
        return redirect("login")->with('error', 'Ошибка');
    }
}
