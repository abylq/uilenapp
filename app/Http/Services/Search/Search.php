<?php

namespace App\Http\Services\Search;
use App\Models\User;
use DB;

class Search implements SearchInterface
{

    private $filterData;
    private $query;

    public function __construct($filterData)
    {
        $this->query = User::leftJoin('user_details', function ($join) {
            $join->on('users.id', '=', 'user_details.user_id');
        })->leftJoin('user_locations', function ($join) {
            $join->on('users.id','=', 'user_locations.user_id');
        })->select('users.id','user_details.date_of_birth','users.name')
        ->where('users.id','!=', auth()->id());


        $this->filterData = $filterData;
    }

    
    public function getQuery()
    {
        return $this->query;
    }

    public function getFilterData()
    {
        return $this->filterData;
    }


}
