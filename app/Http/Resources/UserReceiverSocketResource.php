<?php

namespace App\Http\Resources;

use App\Models\Message;
use App\Models\UserImage;
use Illuminate\Http\Resources\Json\JsonResource;

class UserReceiverSocketResource extends JsonResource
{
    private $sender;
    private $receiver;
    public function __construct($resource,$sender, $receiver)
    {
        parent::__construct($resource);
        $this->sender = $sender;
        $this->receiver = $receiver;
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'avatar' => UserImage::where('user_id', $this->id)->where('avatar',1)->orderBy('id','DESC')->first()->images ?? null,
            'images'  => UserImageResource::collection($this->images),
            'unread_message' => Message::where(fn($q) => $q
                ->where('receiver_id', $this->receiver)
                ->where('sender_id', $this->sender)
                ->where('is_read', 0)
            )->orWhere(fn($q) => $q
                ->where('receiver_id', $this->sender)
                ->where('sender_id', $this->receiver)
                ->where('is_read', 0))
                ->count()
        ];
    }
}
