@extends('admin.layouts.admin')

@section('title',$cat->name)

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <form class="form-horizontal" enctype="multipart/form-data" method="post"
                                  action="{{route('admin.cat.update')}}">
                                @csrf
                                <input type="hidden" name="cat_id" value="{{$cat->id}}">
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 col-form-label">названия</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="name" value="{{$cat->name}}" class="form-control"
                                               id="inputName" placeholder="названия">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger">Сохранить</button>
                                    </div>
                                </div>
                            </form>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>


                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <div class=" row justify-content-between">
                                <h2 class="">Параметры</h2>
                                <a href="{{route('admin.param.create',$cat->id)}}" class="btn btn-primary">добавит параметр</a>
                            </div>

                        </div>
                        <div class="card-body">
                            <table class="table table-bordered">
                                <tr>
                                    <td>ID</td>
                                    <td>Параметр</td>
                                    <td>Значение</td>
                                    <td></td>
                                </tr>
                                @foreach($cat->params as $param)
                                    <tr>
                                        <td>{{$param->id}}</td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>{{$param->name}}</td>
                                                    <td>
                                                        <a href="{{route('admin.param.destroy',$param->id)}}" onclick="return confirm('Вы уверены ?')" class="btn btn-danger">
                                                            <ion-icon name="trash-outline"></ion-icon>
                                                        </a><br><br>

                                                    </td>
                                                    <td>
                                                        <a href="{{route('admin.param.edit'   ,$param->id)}}" class="btn btn-success">
                                                            <ion-icon name="pencil-outline"></ion-icon>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table>
                                                @foreach($param->values as $value)
                                                    <tr>
                                                        <td>{{$value->value}}</td>
                                                        <td>
                                                            <a href="{{route('admin.paramValue.destroy',$value->id)}}" class="btn-sm btn-dark">
                                                                <ion-icon name="trash-outline"></ion-icon>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            </table>
                                        </td>
                                        <td>
                                            <a href="{{route('admin.paramValue.create',$param->id)}}" class="btn btn-dark">
                                                Добавить значение
                                            </a><br><br>

                                        </td>
                                    </tr>
                                @endforeach

                            </table>

                        </div>
                    </div>

                </div>


            </div>
        </div>
    </section>
@endsection
