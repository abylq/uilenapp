<?php

namespace App\Http\Resources;

use App\Models\User;
use App\Models\UserImage;
use Illuminate\Http\Resources\Json\JsonResource;

class WhoLikeUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => User::find($this->from_user_id)->id,
            'name' => User::find($this->from_user_id)->name,
            'avatar' => UserImage::where('user_id', $this->from_user_id)->where('avatar',1)->orderBy('id','DESC')->first()->images ?? null,
            'images'  => UserImageResource::collection(User::find($this->from_user_id)->first()->images),
        ];
    }
}
