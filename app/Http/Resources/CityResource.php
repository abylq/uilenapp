<?php

namespace App\Http\Resources;

use App\Models\City;
use App\Models\Country;
use App\Models\Region;
use Illuminate\Http\Resources\Json\JsonResource;

class CityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $region = Region::where('id', $this->region_id);
        $country = null;
        if ($region->exists()) {
            $country = Country::find($region->first()['country_id']);
        }
        return [
            'id'         => $this->id,
            'name'       => $this->name,
            'country'    => $country  ? new CountryResource($country) : $country,
            'region'     => Region::where('id', $this->region_id)->first()['name'] ?? null,
            'lang'       => $this->lang,
        ];
    }
}
