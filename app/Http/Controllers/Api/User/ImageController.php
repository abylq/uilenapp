<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserImageResource;
use App\Http\Traits\ImageUploader;
use App\Models\UserImage;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use File;
class ImageController extends Controller
{

    use ImageUploader;

    /**
     * @param UserImage $image
     * @return JsonResponse
     */
    public function setAvatar(UserImage $image): JsonResponse
    {
        try {
            if ($image->where('user_id', auth()->id())) {
                $image->update(['avatar' => 1]);
                UserImage::where('user_id', auth()->id())->where('id', '!=', $image->id);
                
            }
        } catch (\Exception $th) {
            return response()->json([
                'message' => 'Повторите еще раз'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }


        return response()->json([
            'data' => new UserImageResource($image)
        ], Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function uploadImages(Request $request): JsonResponse
    {
        $request->validate([
            'images' => 'required',
            'images.*' => 'mimes:jpeg,jpg,png,gif|max:10000'
        ]);
        try {
            if ($request->hasFile('images')) {
                $imageID = [];
                foreach ($request->images as $image) {
                    $userImage = UserImage::create([
                        'user_id' => auth()->id(),
                        'images' => $this->upload($image, 'users/'),
                    ]);
                    $imageID[] = $userImage->id;
                }
            }
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'Unable to upload files'.$th
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        //dd(UserImage::whereIn('id',$imageID)->get());
        return response()->json([
            'data' => UserImageResource::collection(UserImage::whereIn('id',$imageID)->get())
        ], Response::HTTP_OK);
    }


    /**
     * @param UserImage $image
     * @return JsonResponse
     */
    public function destroy(UserImage $image): JsonResponse
    {
        if($image->avatar == 1) {
            $user = UserImage::where('user_id', auth()->id())
            ->where('id', '<', $image->id)
            ->orWhere('id', '>', $image->id)->first();
            $user->update([
                'avatar' => 1
            ]);
        }
        
        if (File::delete($image->images)) {
            $image->delete();
        }else {
            $image->delete();
        }
        
        return response()->json([
            'message' => 'Успешно удалено'
        ], Response::HTTP_OK);
    }

    /**
     * @return JsonResponse
     */
    public function myImages(): JsonResponse
    {
        $user = UserImage::where('user_id', auth()->id())->get();

        return response()->json([
            'data' => UserImageResource::collection($user)
        ]);
    }
}
