<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\UserLikeRequest;
use App\Http\Resources\UsersMatchedResource;
use App\Http\Traits\FCMNotification;
use App\Models\Matching;
use App\Models\Message;
use denis660\Centrifugo\Centrifugo;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class LikeController extends Controller
{   
    use FCMNotification;
    
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function like(UserLikeRequest $request, Centrifugo $centrifugo): JsonResponse|HttpException
    {
        try {
            $centrifugo->info();
        } catch (ConnectException $e) {
            throw new HttpException(503);
        }
        try {
            $checkMatch = Matching::where([
                ['to_user_id', $request->get('to_user_id')],
                ['from_user_id', auth()->id()]
            ]);
            $fromMatch = Matching::where([
                ['from_user_id', $request->get('to_user_id')],
                ['to_user_id', auth()->id()]
            ]);
            if($fromMatch->exists()) {
                if($fromMatch->first()->to_user_id == auth()->id()) {
                    $fromMatch->update([
                        'to_status' => auth()->id(), 
                    ]);
                    if($request->get('status') == 1 || $request->get('superlike') == 1 ) {
                        if($fromMatch->first()->status == 1) {
                            $update = $fromMatch->update([
                                'matched' => 1, 
                            ]); 
                            if ($update) {
                                if (Cache::get('matched_user_id_'.$request->get('to_user_id'))) {
                                    Cache::forget('matched_user_id_'.$request->get('to_user_id'));
                                }
                                Cache::put('matched_user_id_'.$request->get('to_user_id'), 'matched', now()->addMinutes(43800));
                                $centrifugo->publish('signal_'.$request->get('to_user_id'), [
                                    response()->json($this->getSignal($request->get('to_user_id')))->getData()
                                ]);
                                $oneSignal = $this->sendNotificationOneSignal($request->get('to_user_id'),'Взаймно',
                                new UsersMatchedResource($fromMatch->first()), 
                                'matched');
                                return response()->json([
                                    'message' => 'Matched',
                                    'data' => new UsersMatchedResource($fromMatch->first()),
                                    'oneSignal' => $oneSignal
                                ], Response::HTTP_OK);
                            }
                        }  
                    }

                    if($request->get('status') == 2) {
                        $update = $fromMatch->update([
                            'status' => 2, 
                        ]);
                        return response()->json([
                            'message' => 'Success',
                            'data' => new UsersMatchedResource($fromMatch->first()),
                        ], Response::HTTP_OK);
                    }
                    
                }
            }
            if (!$checkMatch->exists()) {
                if (Cache::get('like_user_id_'.$request->get('to_user_id'))) {
                    Cache::forget('like_user_id_'.$request->get('to_user_id'));
                }
                Cache::put('like_user_id_'.$request->get('to_user_id'), 'like', now()->addMinutes(43800));
                $centrifugo->publish('signal_'.$request->get('to_user_id'), [
                    response()->json($this->getSignal($request->get('to_user_id')))->getData()
                ]);
                $create = (new Matching())->likeUser($request);
                $oneSignal = $this->sendNotificationOneSignal($request->get('to_user_id'),'Лайкнул',$create, 'like');
                if($create) {
                    return response()->json([
                        'message' => 'Successfuly created',
                        'data' => new UsersMatchedResource($create),
                        'oneSignal' => $oneSignal
                    ], Response::HTTP_CREATED);
                }
                
            }else {
                $update = $checkMatch->update([
                    'status' => $request->get('superlike') == 1 ? 1 : $request->get('status'),
                    'super_like' =>$request->get('superlike', 0)
                ]); 

                if($update) {
                    return response()->json([
                        'message' => 'Successfuly updated',
                        'data' => new UsersMatchedResource($checkMatch->first()),
                        
                    ], Response::HTTP_CREATED);
                }
            }
        }catch (Throwable $th) {
            return response()->json([
                'error' => $th,
            ]);
        }
        

        return response()->json(null);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updateSignal(Request $request, Centrifugo $centrifugo): JsonResponse|HttpException
    {
        try {
            $centrifugo->info();
        } catch (ConnectException $e) {
            throw new HttpException(503);
        }

        if ($request->get('type') == 'like') {
            if (Cache::get('like_user_id_'.auth()->id())) {
                Cache::forget('like_user_id_'.auth()->id());
            }

            $centrifugo->publish('signal_'.auth()->id(), [
                response()->json($this->getSignal(auth()->id()))->getData()
            ]);
        }

        if ($request->get('type') == 'matched') {
            if (Cache::get('matched_user_id_'.auth()->id())) {
                Cache::forget('matched_user_id_'.auth()->id());
            }
            $centrifugo->publish('signal_'.auth()->id(), [
                response()->json($this->getSignal(auth()->id()))->getData()
            ]);
        }

        return response()->json([
            'message' => 'Sucessfully'
        ]);
    }

    /**
     * @param Centrifugo $centrifugo
     * @return JsonResponse|HttpException
     */
    public function getNotification(Centrifugo $centrifugo): JsonResponse|HttpException
    {
        try {
            $centrifugo->info();
        } catch (ConnectException $e) {
            throw new HttpException(503);
        }

        $centrifugo->publish('signal_'.auth()->id(), [
            response()->json($this->getSignal(auth()->id()))->getData()
        ]);

        return response()->json([
            'message' => 'Successfully'
        ]);
    }
    /**
     * @return array
     */
    private function getSignal($userId): array
    {
        $message = Message::where('receiver_id', $userId)->where('is_read',0)->exists();
        $data = [
            'chat' => (bool)$message,
            'like' => (bool)Cache::get('like_user_id_'.$userId),
            'matched' => (bool)Cache::get('matched_user_id_'.$userId),
        ];
        return $data;
    }



}
