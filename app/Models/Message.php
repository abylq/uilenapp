<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Message extends Model
{
    use HasFactory;


    protected $casts = [
        'created_at'  => 'date:H:i',
        'updated_at'  => 'date:Y-m-d H:i:s',
    ];
    protected $guarded = [];

    /**
     * @return User
     */
    public function getSender(): User
    {
        return $this->belongsTo(User::class,'sender_id')->first();
    }

    /**
     * @return User
     */
    public function getReceiver(): User
    {
        return $this->belongsTo(User::class,'receiver_id')->first();
    }

    /**
     * @return BelongsTo
     */
    public function receiverUser(): BelongsTo
    {
        return $this->belongsTo(User::class,'receiver_id');
    }

    public function senderUser(): BelongsTo
    {
        return $this->belongsTo(User::class,'sender_id');
    }

    public static function report($date)
    {

        switch ($date) {
            case 'month':
                $mesages = self::whereMonth('date_message', Carbon::now()->month);
                return $mesages;
                break;
            case 'week':
                $startOfWeek = Carbon::now()->startOfWeek();
                $endOfWeek = Carbon::now()->endOfWeek();
                $mesages =self::where('date_message', '>=', $startOfWeek)
                    ->where('date_message', '<=', $endOfWeek);
                return $mesages;
                break;
            case 'day':
                $mesages = self::whereDate('date_message', Carbon::now());
                return $mesages;
                break;
        }

    }


}
