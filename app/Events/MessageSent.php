<?php

namespace App\Events;

use App\Http\Resources\MessageResourceSocket;
use App\Models\MessageCallFromDB;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MessageSent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return string
     */
    public function broadcastOn(): string
    {
        return new Channel("dialogs:dialog#{$this->message->sender_id},{$this->message->receiver_id}");
    }

    public function broadcastWith(): array
    {
        return ['message' => response()->json(new MessageResourceSocket($this->message))->getData()];
    }
}
