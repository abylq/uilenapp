@extends('admin.layouts.base')
@section('theme','sidebar-mini')
@section('wrapper')
    <div class="wrapper">

        <!-- Navbar -->
        @include('admin.layouts.navBar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('admin.layouts.sidebar')

        <!-- Content Wrapper. Contains page content -->

        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">@yield('title')</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            @yield('topRightContent')
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
           <div class="content">
               @yield('content')
           </div>
            <!-- /.content -->
        </div>

        <!-- /.content-wrapper -->

{{--        <!-- Control Sidebar -->--}}
{{--        <aside class="control-sidebar control-sidebar-dark">--}}
{{--            <!-- Control sidebar content goes here -->--}}
{{--            <div class="p-3">--}}
{{--                <h5>Title</h5>--}}
{{--                <p>Sidebar content</p>--}}
{{--            </div>--}}
{{--        </aside>--}}
{{--        <!-- /.control-sidebar -->--}}

        <!-- Main Footer -->
        @include('admin.layouts.footer')
    </div>
@endsection
