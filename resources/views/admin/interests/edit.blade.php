@extends('admin.layouts.admin')

@section('title', $interest->title)

@section('content')
    <div class="card">
        <div class="card-body">

            <form action="{{ route('admin.interests.update', $interest->id) }}" enctype="multipart/form-data" method="post">
                @method('put')
                @csrf
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="title" id="name" required class="form-control" value="{{ $interest->title }}">
                </div>

                <div class="form-group">
                    <button class="btn btn-success">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
@endsection
