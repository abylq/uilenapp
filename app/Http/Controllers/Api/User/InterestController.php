<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\InterestResource;
use App\Models\Interest;
use App\Models\UserInterest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class InterestController extends Controller
{


    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json([
            'data' => InterestResource::collection(Interest::all())
        ]);
    }
    /**
     * @param Interest $interest
     * @return JsonResponse
     */
    public function store(Interest $interest): JsonResponse
    {
        UserInterest::create([
            'user_id' => auth()->id(),
            'interest_id' => $interest->id,
        ]);

        return response()->json([
            'message' => 'Успешно добавлено'
        ], Response::HTTP_OK);
    }

    /**
     * @param Interest $interest
     * @return JsonResponse
     */
    public function destroy(Interest $interest): JsonResponse
    {
        if ($userInterest = UserInterest::where(['user_id' => auth()->id(), 'interest_id' => $interest->id])) {
            $userInterest->delete();

            return response()->json([
                'message' => 'Успешно удалено'
            ], Response::HTTP_OK);
        }
        return response()->json([
            'message' => 'Not found user or interest'
        ], Response::HTTP_NOT_FOUND);
    }
}
