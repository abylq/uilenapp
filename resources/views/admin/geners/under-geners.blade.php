@extends('admin.layouts.admin')
@section('title', $geners->name)
@section('topRightContent')
    <a href="{{ route('admin.geners.create') }}" class="btn btn-primary float-sm-right">Добавить </a>

    <a href="{{ route('admin.geners.create', 'create=under-geners') }}" class="btn btn-success float-sm-right mr-1">Добавить под ру</a>
@endsection
@section('content')
    <div class="card">
        <div class="card-body p-0">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Название</th>
                    <th>Ру</th>
                    <th >действие</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($geners->geners as $item)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $item->name }}</td>

                             <td>
                                 {{ $geners->name }}
                             </td>
                            <td >
                                <a href="{{ route('admin.geners.edit', $item->id) }}" class="btn btn-warning">
                                    <ion-icon name="pencil-outline"></ion-icon>
                                </a>

                                <a href="{{ route('admin.geners.delete', $item->id) }}" onclick="return confirm('Вы уверены ?')" class="btn btn-danger">
                                    <ion-icon name="trash-outline"></ion-icon>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>


        </div>
    </div>
@endsection
