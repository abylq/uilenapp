<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Laravel\Sanctum\PersonalAccessToken;

class VerifyController extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function sendCode(Request $request): JsonResponse
    {
        $request->validate([
            'phone' => 'required'
        ]);
        $user = User::where('phone', $request->get('phone'))->first();

        if ($user) {
            $data = ['phone' => $request->get('phone'),'code'  => 1111];
            Cache::put('cash_phone', $data, now()->addMinutes(2));
            return response()->json([
                'message' => 'СМС отправлено',
                'data' => new UserResource($user)
            ],Response::HTTP_OK);
        }
        return response()->json([
            'message' => 'Ошибка! При отправке смс',
        ],Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param User $user
     * @param Request $request
     * @return JsonResponse
     */
    public function verifyPhone(?User $user, Request $request): JsonResponse
    {
        $request->validate([
            'code' => 'required'
        ]);

        if (Cache::get('cash_phone')['code'] == $request->code) {

            if ($user) {
                $user->update(['phone_verified' => Str::uuid()->toString()]);
                return response()->json([
                    'token' => $user->createToken('user-token')->plainTextToken,
                ],Response::HTTP_OK);
            }
        }

        
        return response()->json([
            'message' => 'Неправильный код.'
        ],Response::HTTP_UNAUTHORIZED);
    }


        /**
     * @param Request $request
     * @return JsonResponse
     */
    public function sendCodeToNewNumber(Request $request): JsonResponse
    {
        $request->validate([
            'phone' => 'required'
        ]);
        $data = ['phone' => $request->get('phone'),'code'  => 1111];

        if(User::where('phone', $data['phone'])->exists()) {
            return response()->json([
                'message' => 'Пользователь с таким номером уже существует!',
            ],Response::HTTP_BAD_REQUEST);
        }

        Cache::put('cash_phone', $data, now()->addMinutes(2));
        
        return response()->json([
            'message' => 'СМС отправлено',
        ],Response::HTTP_OK);
    }
}
