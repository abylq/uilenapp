@extends('admin.layouts.admin')
@section('title','Категория')
@section('topRightContent')
    <a href="{{route('admin.cat.create')}}" class="btn btn-primary float-sm-right">
        Добавить  категорю
    </a>
@endsection
@section('content')
    <div class="card">
        <div class="card-body p-0">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th></th>
                    <th>Продукты</th>
                    <th>действие</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cats as $cat)
                    <tr>
                        <td>{{$cat->id}}</td>
                        <td>{{$cat->name}}</td>
                        <td>{{$cat->product_count}}</td>
                        <td >
                            <a href="{{route('admin.product.index',['cat_id'=>$cat->id])}}" class="btn btn-primary">
                                <ion-icon name="open-outline"></ion-icon>
                            </a>
                            <a href="{{route('admin.cat.edit',$cat->id)}}" class="btn btn-warning">
                                <ion-icon name="pencil-outline"></ion-icon>
                            </a>
                            <a href="{{route('admin.cat.destroy',$cat->id)}}" onclick="return confirm('Вы уверены ?')" class="btn btn-danger">
                                <ion-icon name="trash-outline"></ion-icon>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>


        </div>
    </div>
@endsection
