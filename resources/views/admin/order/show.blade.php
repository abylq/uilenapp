@extends('admin.layouts.admin')

@section('title')
   заказ №{{$order->id}}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>ФИО</b> <a class="float-right" href="{{route('admin.user.show',$user->id)}}">
                                {{$user->name}}
                            </a>
                        </li>
                        <li class="list-group-item">
                            <b>телефон номер</b> <a class="float-right">{{$user->phone}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Дата</b> <a class="float-right">{{\Carbon\Carbon::parse($order->created_at)->format('Y-m-d')}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Время</b> <a class="float-right">{{\Carbon\Carbon::parse($order->created_at)->format('h:i')}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Общая сумма</b> <a class="float-right">{{$price}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('admin.order.update')}}" method="post">
                        @csrf
                        <input type="hidden" name="basket_id" value="{{$order->id}}">
                        <div class="form-group">
                            <label for="status">Статус</label>
                            <select class="form-control" name="status" id="status">
                                <option {{$order->status == 'error' ? 'selected' : ''}} value="error">Ошибка</option>
                                <option {{$order->status == 'payment' ? 'selected' : ''}} value="payment">Оплачено</option>
                                <option {{$order->status == 'accept' ? 'selected' : ''}} value="accept">Заказ принят</option>
                                <option {{$order->status == 'preparing' ? 'selected' : ''}} value="preparing">Подготовка заказа</option>
                                <option {{$order->status == 'courier' ? 'selected' : ''}} value="courier">Передан курьеру</option>
                                <option {{$order->status == 'delivered' ? 'selected' : ''}} value="delivered">Доставлен</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success" type="submit">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Название </th>
                            <th>количество шт</th>
                            <th>цена</th>
                            <th>сумма</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$product->name}}</td>
                                <td>{{$product->count}}</td>
                                <td>{{$product->price}} тг</td>
                                <td>{{$product->price * $product->count}} тг</td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
