<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use denis660\Centrifugo\Centrifugo;
use Illuminate\Http\JsonResponse;

class CentrifugoController extends Controller
{

    /**
     * @param Centrifugo $centrifugo
     * @return JsonResponse
     */
    public function __invoke(Centrifugo $centrifugo): JsonResponse
    {
        return response()->json(['token' => $centrifugo->generateConnectionToken(auth()->user()->id)]);
    }
}
