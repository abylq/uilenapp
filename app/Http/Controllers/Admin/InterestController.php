<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Interest;
use Illuminate\Http\Request;

class InterestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interest = Interest::all();

        return view('admin.interests.index', [
            'interests' => $interest
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.interests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required'
        ]);

        if (Interest::create(['title' => $request->title])) {
            return redirect()->back()->with('success','Успешно добавлено');
        }

        return redirect()->back()->with('error','Повторите еще раз !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Interest $interest
     */
    public function edit(Interest $interest)
    {
        return view('admin.interests.edit',[
            'interest' => $interest
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Interest $interest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Interest $interest)
    {

        $request->validate([
            'title' => 'required'
        ]);

        if ($interest) {
            $interest->update(['title' => $request->title]);

            return redirect()->back()->with('success','Успешно обновлено');
        }

        return redirect()->back()->with('error','Повторите еще раз');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $interest = Interest::find($id);
        if ($interest) {
            $interest->delete();
            return redirect()->back()->with('success','Успешно удалено');
        }
        return redirect()->back()->with('error','Попробуйте позже');

    }
}
